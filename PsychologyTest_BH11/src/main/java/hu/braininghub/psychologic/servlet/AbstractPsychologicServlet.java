/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import javax.servlet.http.HttpServlet;

/**
 *
 * @author Marton Petrekanics
 */
public abstract class AbstractPsychologicServlet extends HttpServlet {

    public static final String CATEGORY_ADDED = "A kategória létrehozva: ";
    public static final String CATEGORY_ADDITITON_FAILED
            = "A kategória létrehozása sikertelen, valószínűleg már létezik.";
    public static final String QUESTIONNAIRE_CREATED = "A kérdőív létrehozva: ";
    public static final String QUESTIONNAIRE_CREATION_FAILED
            = "A kérdőív létrehozása sikertelen, valószínűleg már létezik.";
    public static final String QUESTION_CREATED = "A kérdés létrehozva: ";
    public static final String QUESTION_CREATION_FAILED
            = "A kérdés létrehozása sikertelen, valószínűleg már létezik.";
    public static final String USER_ADDED = "A felhasználófiók létrehozva: ";
    public static final String USER_CREATION_FAILED 
            = "A felhasználófiók létrehozása sikertelen.";
    public static final String FEEDBACK_ADDED = "A feedback elküldve! Köszönjük a visszajelzésedet!";
    public static final String FEEDBACK_CREATION_FAILED 
            = "A feedback beküldése sikertelen!";
}
