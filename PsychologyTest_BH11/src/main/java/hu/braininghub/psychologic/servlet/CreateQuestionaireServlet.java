/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.service.PsychologyService;
import hu.braininghub.psychologic.validation.QuestionaireDtoValidator;
import hu.braininghub.psychologic.validation.ValidationException;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 * CreateQServlet
 *
 * @author Aliz
 */
@Slf4j
@WebServlet(name = "CreateQuestionaireServlet", urlPatterns = "/create-questionnaire")
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "expert"}))
public class CreateQuestionaireServlet extends AbstractPsychologicServlet {

    @Inject
    private PsychologyService psychologyService;

    @Inject

    private QuestionaireDtoValidator questionaireDTOValidator;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<CategoryDto> categoryDTO = psychologyService.getAllCategories();
        request.setAttribute("categories", categoryDTO);
        request.getRequestDispatcher("WEB-INF/create_questionaire.jsp").forward(request, response);

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String shortDesc = request.getParameter("short-desc");
        String longDesc = request.getParameter("long-desc");
        String extraQuestion = request.getParameter("extra-question");
        String categoryId = request.getParameter("category");

        QuestionaireDto questionaireDTO = new QuestionaireDto();
        questionaireDTO.setName(name);
        questionaireDTO.setShortDescription(shortDesc);
        questionaireDTO.setLongDescription(longDesc);
        questionaireDTO.setExtraQuestions(extraQuestion);

        try {
            Integer id = Integer.valueOf(categoryId);
            CategoryDto category = new CategoryDto();
            category.setId(id);
            questionaireDTO.setCategoryDTO(category);
        } catch (NumberFormatException e) {
            log.debug("Set category to" + questionaireDTO.getName() + " questionaire was unsuccesful: ", e);
            //nothing, parsing id is not successful
        }

        //similar to CreateCategoryServlet
        String msg = null;
        boolean error = false;
        try {
            this.questionaireDTOValidator.isValid(questionaireDTO);

            if (psychologyService.addQuestionaire(questionaireDTO)) {
                msg = QUESTIONNAIRE_CREATED + questionaireDTO.getName();
                log.info("Questionnaire has been created: " + questionaireDTO.getName());
            } else {
                msg = QUESTIONNAIRE_CREATION_FAILED;
                error = true;
                log.warn("Failure! Questionnaire has not been created: " + questionaireDTO.getName());
            }
        } catch (ValidationException e) {
            msg = e.getMessage();
            error = true;
        }

        request.setAttribute("isError", error);
        request.setAttribute("msg", msg);

        this.doGet(request, response);
    }

    //needed for tests
    public void setPsychologyService(PsychologyService psychologyService) {
        this.psychologyService = psychologyService;
    }

    public void setQuestionaireDTOValidator(QuestionaireDtoValidator questionaireDTOValidator) {
        this.questionaireDTOValidator = questionaireDTOValidator;
    }
}
