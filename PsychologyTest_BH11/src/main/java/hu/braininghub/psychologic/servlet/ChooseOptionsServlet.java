/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.AnswerDemoDto;
import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.model.OptionDemo;
import hu.braininghub.psychologic.service.DemoService;
import hu.braininghub.psychologic.service.PsychologyService;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aliz
 */
@WebServlet(name = "ChooseOptionsServlet", urlPatterns = "/choose-options")
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "expert", "user"}))
public class ChooseOptionsServlet extends HttpServlet {

    @Inject
    private PsychologyService service;

    @Inject
    private DemoService demoService;

    private QuestionaireDto handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer id = 0;
        try {
            String rawId = request.getParameter("id");
            id = Integer.valueOf(rawId);
        } catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);   //404
            return null;
        }
        QuestionaireDto questionaireDto = service.getQuestionnaireById(id);
        return questionaireDto;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        QuestionaireDto questionaireDto = handleRequest(request, response);
        if (questionaireDto == null) {
            return;
        }

        Optional<QuestionDto> quOptional = demoService.getNextQuestionDemo(questionaireDto.getId());
        request.setAttribute("next_question", quOptional.orElse(null));
        request.setAttribute("answers", demoService.getAllAnswerDemo(questionaireDto.getId()));
        request.setAttribute("options", Arrays.asList(OptionDemo.values()));

        request.setAttribute("questionaire", questionaireDto);
        request.getRequestDispatcher("WEB-INF/choose_options.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer questionId = 0;

        try {
            String rawId = request.getParameter("question");
            questionId = Integer.valueOf(rawId);

        } catch (NumberFormatException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);   //404
            return;
        }

        OptionDemo optionDemo = null;
        try {
            optionDemo = optionDemo.valueOf(request.getParameter("answer"));
        } catch (IllegalArgumentException | NullPointerException e) {
            request.setAttribute("isError", true);
            request.setAttribute("msg", "Kérem válasszon ki egy opciót a választási lehetőségek közül!");
            doGet(request, response);
            return;
        }

        QuestionDto questionDto = service.getQuestionById(questionId);
        if (questionDto == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        boolean error = false;
        String msg = "";

        AnswerDemoDto answerDemo = new AnswerDemoDto();
        answerDemo.setOption(optionDemo);
        answerDemo.setQuestionDto(questionDto);
        if (demoService.addAnswerDemo(answerDemo)) {
            msg = "A válasz hozzáadása sikerült :)";
        } else {
            error = true;
            msg = "A válasz hozzáadása nem sikerült :(";
        }

        request.setAttribute("isError", error);
        request.setAttribute("msg", msg);

        doGet(request, response);

    }
}
