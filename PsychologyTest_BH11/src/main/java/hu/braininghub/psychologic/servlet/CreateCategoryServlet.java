/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.service.PsychologyService;
import hu.braininghub.psychologic.validation.CategoryDtoValidator;
import hu.braininghub.psychologic.validation.ValidationException;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Aliz
 */
@Slf4j
@WebServlet(name = "CreateCategoryServlet", urlPatterns = "/create-category")
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "expert"}))
public class CreateCategoryServlet extends AbstractPsychologicServlet {

    @Inject
    private CategoryDtoValidator categoryDTOValidator;

    @Inject
    private PsychologyService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/create_cat.jsp").forward(request, response);

    }

    /**
     * Process category from form
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String categoryName = req.getParameter("name");
        boolean error = false;
        String msg = "";

        CategoryDto categoryDTO = new CategoryDto();
        categoryDTO.setName(categoryName);

        try {
            this.categoryDTOValidator.isValid(categoryDTO);

            if (this.service.addCategory(categoryDTO)) {
                msg = CATEGORY_ADDED + categoryDTO.getName();
                log.info("Category has been created: " + categoryDTO.getName());
            } else {
                msg = CATEGORY_ADDITITON_FAILED;
                error = true;
                log.warn("Failure! Category has not been created: " + categoryDTO.getName());
            }
        } catch (ValidationException e) {
            msg = e.getMessage();
            error = true;
        }

        req.setAttribute("isError", error);
        req.setAttribute("msg", msg);

        this.doGet(req, resp);
    }
}
