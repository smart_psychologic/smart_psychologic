/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.service.Evaluation2Service;
import hu.braininghub.psychologic.service.EvaluationService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marton Petrekanics
 */
@WebServlet(name = "EvaluationServlet", urlPatterns = "/evaluation")
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "expert", "user"}))
public class EvaluationServlet extends AbstractPsychologicServlet {

    @Inject
    private EvaluationService service;

    @Inject
    private Evaluation2Service service2;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = "";
        try {
            name = request.getUserPrincipal().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String result = "Ön még nem töltött ki kérdőívet. ";
        String questionnaireName = "";
        try {
            int filloutId = service2.findFilloutForUserName(name);
            result = service.evaluateQuestionnaire(filloutId);
            questionnaireName = service2.findQuestionnaireNameForFillout(filloutId);
            questionnaireName = "(" + questionnaireName + ")";
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.setAttribute("result", result);
        request.setAttribute("questionnaireName", questionnaireName);
        request.getRequestDispatcher("/WEB-INF/evaluation.jsp").forward(request, response);

    }
}
