/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.service.PsychologyService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aliz
 */
@WebServlet(name = "CategoriesServlet", urlPatterns = "/categories")
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "expert", "user"}))
public class CategoriesServlet extends AbstractPsychologicServlet {

    @Inject
    private PsychologyService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("categories", this.service.getAllCategories());
        request.getRequestDispatcher("WEB-INF/categories.jsp").forward(request, response);

    }
}
