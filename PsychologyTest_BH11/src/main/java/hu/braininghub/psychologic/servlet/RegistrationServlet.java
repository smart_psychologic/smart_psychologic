/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.UserDto;
import hu.braininghub.psychologic.service.UserService;
import hu.braininghub.psychologic.validation.UserDtoValidator;
import hu.braininghub.psychologic.validation.ValidationException;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Aliz
 */
@Slf4j
@WebServlet(name = "RegistrationServlet", urlPatterns = "/registration")
public class RegistrationServlet extends AbstractPsychologicServlet {

    @Inject
    UserDtoValidator userDtoValidator;

    @Inject
    UserService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("WEB-INF/registration.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDto userDto = new UserDto();
        userDto.setName(req.getParameter("name"));
        userDto.setEmailAddress(req.getParameter("emailAddress"));
        userDto.setPassword(req.getParameter("password"));
        userDto.setLastName(req.getParameter("lastName"));
        userDto.setFirstName(req.getParameter("firstName"));

        boolean error = false;
        String msg = "";

        try {
            this.userDtoValidator.isValid(userDto);

            if (this.service.addUser(userDto)) {
                msg = USER_ADDED + userDto.getName();
                log.info("User has been created: " + userDto.getName());
            } else {
                msg = USER_CREATION_FAILED;
                error = true;
                log.warn("Failure! User has not been created: " + userDto.getName());
            }
        } catch (ValidationException e) {
            msg = e.getMessage();
            error = true;
        }

        req.setAttribute("isError", error);
        req.setAttribute("msg", msg);

        this.doGet(req, resp);
    }
}
