/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.controller.AnswerDao;
import hu.braininghub.psychologic.controller.CategoryDao;
import hu.braininghub.psychologic.controller.DimensionDao;
import hu.braininghub.psychologic.controller.FilloutDao;
import hu.braininghub.psychologic.controller.OptionDao;
import hu.braininghub.psychologic.controller.QuestionDao;
import hu.braininghub.psychologic.controller.QuestionDimensionDao;
import hu.braininghub.psychologic.controller.QuestionaireDao;
import hu.braininghub.psychologic.service.EvaluationService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Marton Petrekanics
 */
@WebServlet(name = "ShouldBeDeletedServlet", urlPatterns = "/tryout")
public class ShouldBeDeletedServlet extends AbstractPsychologicServlet {

    @Inject
    EvaluationService evaluator;

    @Inject
    FilloutDao filloutDao;

    @Inject
    CategoryDao categoryDao;

    @Inject
    QuestionaireDao questionaireDao;

    @Inject
    QuestionDao questionDao;

    @Inject
    AnswerDao answerDao;

    @Inject
    DimensionDao dimensionDao;

    @Inject
    QuestionDimensionDao questionDimensionDao;

    @Inject
    OptionDao optionDao;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("testing: " + categoryDao.listAllCategories().toString());
        System.out.println("testing new dao method: " + filloutDao.getQuestionnaireIdForFillout(1));
        System.out.println("testing new dao method: " + filloutDao.getQuestionnaireIdForFillout(2));
        System.out.println("testing all questionaires: " + questionaireDao.getAllQuestionaires().toString());
        System.out.println("get all questions for questionaire 2: "
                + questionDao.getAllQuestionIdsForQuestionnaire(2).toString());
        System.out.println("get answers for fillout 2: " + answerDao.getAnswersForFillout(2).toString());

        System.out.println("get dimensions for questionnaire 2: "
                + dimensionDao.getAllDimensionsForQuestionnaire(2).toString());
        System.out.println("get all question dimensions for question number 10: "
                + questionDimensionDao.getAllQuestionDimensionsForQuestion(10).toString());
        System.out.println("get options for questionnaire 2: "
                + optionDao.getOptionsForQuestionnaire(2).toString());

        // System.out.println("evaluator tries the same for optionDtos: " + 
        //       evaluator.provideOptionDTOs(2).toString());
        try {
            System.out.println("evaluation for fillout=1: " + evaluator.evaluateQuestionnaire(1));
            System.out.println("evaluation for fillout=2: " + evaluator.evaluateQuestionnaire(2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
