/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.FeedbackDto;
import hu.braininghub.psychologic.service.FeedbackService;
import hu.braininghub.psychologic.validation.FeedbackDtoValidator;
import hu.braininghub.psychologic.validation.ValidationException;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author mito_qa
 */
@Slf4j
@WebServlet(name = "ContactServlet", urlPatterns = "/contact")
public class ContactServlet extends AbstractPsychologicServlet {

    @Inject
    private FeedbackService service;
    
    @Inject
    private FeedbackDtoValidator validator;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("contact.jsp").forward(request, response);

    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String country = req.getParameter("country");
        String message = req.getParameter("message");
        
        FeedbackDto feedbackDto = new FeedbackDto();
        feedbackDto.setFirstName(firstName);
        feedbackDto.setLastName(lastName);
        feedbackDto.setCountry(country);
        feedbackDto.setMessage(message);
        
        boolean error = false;
        String msg = null;


        try {
            validator.isValid(feedbackDto);

            if (service.addFeedback(feedbackDto)) {
                msg = FEEDBACK_ADDED;
                log.info("Feedback has been created: " + feedbackDto.getId());
            } else {
                msg = FEEDBACK_CREATION_FAILED;
                error = true;
                log.warn("Failure! Feedback has not been created: ");
            }
        } catch (ValidationException e) {
            msg = e.getMessage();
            error = true;
        }

        req.setAttribute("isError", error);
        req.setAttribute("msg", msg);

        this.doGet(req, resp);
    }
}
