/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.service.PsychologyService;
import hu.braininghub.psychologic.tool.ExtensionConverter;
import hu.braininghub.psychologic.validation.QuestionDtoValidator;
import hu.braininghub.psychologic.validation.ValidationException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Aliz
 */
@Slf4j
@WebServlet(name = "QuestionaireDetailsServlet", urlPatterns = {"/questionaire-details"})
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "expert", "user"}))
@MultipartConfig
public class QuestionaireDetailsServlet extends HttpServlet {

    private static final String QUESTION_CREATED = "A kérdés létrehozva: ";
    private static final String QUESTION_CREATION_FAILED = "A kérdés létrehozása sikertelen, valószínűleg már létezik.";

    @Inject
    private PsychologyService service;

    @Inject
    private QuestionDtoValidator questionDTOValidator;

    @Inject
    private ExtensionConverter converter;

    private QuestionaireDto handleRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer id = 0;
        try {
            String rawId = request.getParameter("id");
            id = Integer.valueOf(rawId);
        } catch (NumberFormatException e) {
            log.debug("Set rawId was unsuccesful: ", e);
            response.sendError(HttpServletResponse.SC_NOT_FOUND);   //404
            return null;
        }
        QuestionaireDto questionaireDTO = service.getQuestionnaireById(id);
        return questionaireDTO;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        QuestionaireDto questionaireDTO = handleRequest(request, response);
        if (questionaireDTO == null) {
            return;
        }

        request.setAttribute("questionaire", questionaireDTO);
        request.getRequestDispatcher("WEB-INF/view_question_list.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //here we retreive the questionaire
        QuestionaireDto questionaireDTO = handleRequest(request, response);
        if (questionaireDTO == null) {
            return;
        }

        String text = request.getParameter("text");

        String rawRank = request.getParameter("rank");
        Integer rank = null;
        try {
            rank = Integer.valueOf(rawRank);
        } catch (NumberFormatException e) {
            log.debug("Set rawRank was unsuccesful: ", e);
        }

        Part mediaPart = request.getPart("media");
        //path of the media file
        String media = null;
        String mediaType = null;
        if (mediaPart != null && mediaPart.getSize() > 1) {
            //the name of the file should be always unique
            String randomName = UUID.randomUUID().toString();

            // Get the temporary directory and print it.
            String tempDir = System.getProperty("java.io.tmpdir");
            //create a file with temp directory, random name
            File outputFile = new File(tempDir, randomName);
            log.info("New media: " + outputFile.getAbsolutePath());
            media = outputFile.getAbsolutePath();
            mediaType = converter.toImageType(mediaPart.getSubmittedFileName());

            OutputStream outputStream = new FileOutputStream(outputFile);
            //converting input content to output
            IOUtils.copy(mediaPart.getInputStream(), outputStream);
            outputStream.close();

        }

        String rawAlternativeNext = request.getParameter("alternativeNext");
        Integer alternativeNext = null;
        try {
            alternativeNext = Integer.valueOf(rawAlternativeNext);
        } catch (NumberFormatException e) {
            log.debug("Set alternativeNext was unsuccesful: ", e);
        }

        QuestionDto questionDto = new QuestionDto();
        questionDto.setText(text);
        questionDto.setRank(rank);
        questionDto.setMedia(media);
        questionDto.setMediaType(mediaType);
        questionDto.setAlternativeNext(alternativeNext);
        questionDto.setQuestionnaireDto(questionaireDTO);

        String msg = null;
        boolean error = false;
        try {
            this.questionDTOValidator.isValid(questionDto);

            if (service.addQuestion(questionDto)) {
                msg = QUESTION_CREATED + questionDto.getText();
                log.info("Question has been created: " + questionDto.getText());
            } else {
                msg = QUESTION_CREATION_FAILED;
                error = true;
                log.warn("Failure! Question has not been created: " + questionDto.getText());
            }
        } catch (ValidationException exception) {
            msg = exception.getMessage();
            error = true;
        }

        request.setAttribute("isError", error);
        request.setAttribute("msg", msg);

        doGet(request, response);
    }
}
