/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.service.FeedbackService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mito_qa
 */
@WebServlet(name = "FeedbackServlet", urlPatterns = "/feedbacks")
@ServletSecurity(@HttpConstraint(rolesAllowed = {"admin", "expert"}))
public class FeedbackServlet extends AbstractPsychologicServlet {
    
    @Inject
    private FeedbackService service;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setAttribute("feedbacks", service.getAllFeedbacks());
        request.getRequestDispatcher("WEB-INF/feedbacks.jsp").forward(request, response);

    }
}
