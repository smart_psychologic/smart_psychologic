/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.service.PsychologyService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * choose-questionaire
 *
 * @author Aliz
 */
@WebServlet(name = "ChooseQuestionaireServlet", urlPatterns = {"/choose-questionaire"})
@ServletSecurity(
        @HttpConstraint(rolesAllowed = {"admin", "expert", "user"}))
public class ChooseQuestionaireServlet extends HttpServlet {

    @Inject
    private PsychologyService service;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<QuestionaireDto> questionaireDtos = service.getAllQuestionaires();

        request.setAttribute("q_dtos", questionaireDtos);
        request.getRequestDispatcher("WEB-INF/choose_questionaire.jsp").forward(request, response);
    }
}
