/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.service.PsychologyService;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Aliz
 */
@WebServlet(name = "ShowImageServlet", urlPatterns = {"/question-image"})
public class ShowImageServlet extends HttpServlet {

    @Inject
    private PsychologyService psychologyService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Integer id = null;
        try {
            String rawId = request.getParameter("id");
            id = Integer.valueOf(rawId);
        } catch (NumberFormatException exception) {
            //do nothing
        }
        QuestionDto questionDto = psychologyService.getQuestionById(id);
        if (questionDto == null || questionDto.getMedia() == null || questionDto.getMediaType() == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);   //404
            return;
        }

        File image = new File(questionDto.getMedia());
        if (!image.exists()) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        response.setContentType(questionDto.getMediaType());

        //reading the file which was saved
        FileInputStream imgInputStream = new FileInputStream(image);
        OutputStream outputStream = response.getOutputStream();
        IOUtils.copy(imgInputStream, outputStream);
        outputStream.close();
    }

}
