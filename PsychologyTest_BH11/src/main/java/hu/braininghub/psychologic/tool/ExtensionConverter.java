/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.tool;

import javax.ejb.Singleton;
import org.apache.commons.io.FilenameUtils;

/**
 * Based on file extension we create a file type by which we can view the file
 *
 * @author Aliz
 */
@Singleton
public class ExtensionConverter {

    //mimeType
    public String toImageType(String fileName) {
        if (fileName == null) {
            return null;
        }

        //https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
        String extension = FilenameUtils.getExtension(fileName);
        switch (extension.toLowerCase()) {
            case "jpg":
                return "image/jpeg";
            case "png":
                return "image/png";
            default:
                return null; //other, not supported
        }
    }
}
