/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import com.google.common.hash.Hashing;
import hu.braininghub.psychologic.controller.UserDao;
import hu.braininghub.psychologic.dto.UserDto;
import hu.braininghub.psychologic.mapper.UserMapper;
import hu.braininghub.psychologic.model.User;
import java.nio.charset.StandardCharsets;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Aliz
 */
@Slf4j
@Singleton
public class UserService {

    @Inject
    UserDao userDao;

    @Inject
    UserMapper userMapper;

    public boolean addUser(UserDto userDto) {
        try {
            User user = userMapper.toEntity(userDto);
            //https://www.baeldung.com/sha-256-hashing-java
            String sha256hex = Hashing.sha256().hashString(user.getPassword(), StandardCharsets.UTF_8).toString();
            user.setPassword(sha256hex);
            userDao.addUser(user);
            log.info("New user has been added: " + userDto);
        } catch (Exception e) {
            log.debug("Cannot create new user" + e);
            return false;
        }
        return true;
    }
}
