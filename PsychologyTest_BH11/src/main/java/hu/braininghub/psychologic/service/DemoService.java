/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.controller.DemoDao;
import hu.braininghub.psychologic.dto.AnswerDemoDto;
import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.mapper.AnswerDemoMapper;
import hu.braininghub.psychologic.mapper.QuestionMapper;
import hu.braininghub.psychologic.model.AnswerDemo;
import hu.braininghub.psychologic.model.Question;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Aliz
 */
@Singleton
@Slf4j
public class DemoService {

    @Inject
    private DemoDao dao;

    @Inject
    private AnswerDemoMapper answerDemoMapper;

    @Inject
    private QuestionMapper questionMapper;

    public Optional<QuestionDto> getNextQuestionDemo(Integer questionaireId) {
        Optional<Question> queOptional = dao.getNextQuestionDemo(questionaireId);
        if (!queOptional.isPresent()) {
            return Optional.empty();
        }

        QuestionDto questionDto = questionMapper.toDto(queOptional.get());
        return Optional.of(questionDto);
    }

    public List<AnswerDemoDto> getAllAnswerDemo(Integer questionaireId) {
        return dao.getAllAnswerDemo(questionaireId).stream()
                .map(answer -> answerDemoMapper.toDto(answer))
                .collect(Collectors.toList());
    }

    public boolean addAnswerDemo(AnswerDemoDto answerDemoDto) {
        try {
            AnswerDemo answerDemo = answerDemoMapper.toEntity(answerDemoDto);
            dao.addAnswerDemo(answerDemo);
            log.info("New answer added: " + answerDemo);

        } catch (Exception e) {
            log.error("Cannot create new answer", e);
            return false;
        }
        return true;
    }
}
