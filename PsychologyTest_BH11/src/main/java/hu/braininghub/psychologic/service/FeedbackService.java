/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.controller.FeedbackDao;
import hu.braininghub.psychologic.dto.FeedbackDto;
import hu.braininghub.psychologic.mapper.FeedbackMapper;
import hu.braininghub.psychologic.model.Feedback;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author mito_qa
 */
@Slf4j
@Singleton
@Setter
public class FeedbackService {
    
    @Inject
    private FeedbackMapper feedbackMapper;
    
    @Inject
    private FeedbackDao feedbackDao;
    
    public boolean addFeedback(FeedbackDto feedbackDto) {
        try {
            Feedback feedback = feedbackMapper.toEntity(feedbackDto);
            feedbackDao.save(feedback);
        } catch (Exception e) {
            log.debug("Cannot add feedback", e);
            return false;
        }
        return true;
    }
    
    public List<FeedbackDto> getAllFeedbacks(){
        try {
            return feedbackDao.findAll()
                    .stream()
                    .map(f -> feedbackMapper.toDto(f))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.debug("Cannot list all feedback", e);
            return new ArrayList<>();
        }
    }
    
}
