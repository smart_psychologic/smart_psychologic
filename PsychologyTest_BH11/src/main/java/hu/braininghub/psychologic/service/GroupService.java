/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.controller.GroupDao;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author Aliz
 */
@Singleton
public class GroupService {

    @Inject
    private GroupDao dao;

    public List<String> findGroups(String username) {
        return dao.getGroups(username);
    }
}
