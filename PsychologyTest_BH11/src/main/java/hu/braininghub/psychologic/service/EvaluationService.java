/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.controller.AnswerDao;
import hu.braininghub.psychologic.controller.DimensionDao;
import hu.braininghub.psychologic.controller.FilloutDao;
import hu.braininghub.psychologic.controller.OptionDao;
import hu.braininghub.psychologic.controller.QuestionDao;
import hu.braininghub.psychologic.controller.QuestionDimensionDao;
import hu.braininghub.psychologic.controller.UserDao;

import hu.braininghub.psychologic.mapper.AnswerMapper;
import hu.braininghub.psychologic.mapper.DimensionMapper;
import hu.braininghub.psychologic.mapper.OptionMapper;
import hu.braininghub.psychologic.mapper.QuestionDimensionMapper;

import hu.braininghub.psychologic.dto.AnswerDto;
import hu.braininghub.psychologic.dto.DimensionDto;
import hu.braininghub.psychologic.dto.OptionDto;
import hu.braininghub.psychologic.dto.QuestionDimensionDto;

import hu.braininghub.psychologic.model.Answer;
import hu.braininghub.psychologic.model.Dimension;
import hu.braininghub.psychologic.model.Option;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Stateless
@Setter
public class EvaluationService {

    @Inject
    private QuestionDao questionDao;

    @Inject
    private AnswerDao answerDao;

    @Inject
    private DimensionDao dimensionDao;

    @Inject
    private QuestionDimensionDao questionDimensionDao;

    @Inject
    private OptionDao optionDao;

    @Inject
    private FilloutDao filloutDao;

    @Inject
    private UserDao userDao;

    @Inject
    private OptionMapper optionMapper;

    @Inject
    private QuestionDimensionMapper questionDimensionMapper;

    @Inject
    private AnswerMapper answerMapper;

    @Inject
    private DimensionMapper dimensionMapper;

    public String evaluateQuestionnaire(int filloutId) throws EvaluationException {
        String resultText = "";
        int questionnaireId = filloutDao.getQuestionnaireIdForFillout(filloutId)
                .orElseThrow(() -> new EvaluationException("Missing questionnaireId."));
        List<AnswerDto> answerDTOs = provideAnswerDTOs(filloutId);
        List<DimensionDto> dimensionDTOs = provideDimensionDTOs(questionnaireId);
        List<Integer> questionIds = questionDao.getAllQuestionIdsForQuestionnaire(questionnaireId);

        List<QuestionDimensionDto> questionDimensionDTOs
                = provideQuestionDimensionDtosForAQuestion(questionIds);
        List<OptionDto> optionDTOs = provideOptionDTOs(questionnaireId);
        Map<DimensionDto, Integer> map = calculateSumsForDimensions(answerDTOs,
                optionDTOs, dimensionDTOs, questionDimensionDTOs);
        resultText = createFullString(map, dimensionDTOs);
        return resultText;
    }

    private List<QuestionDimensionDto> provideQuestionDimensionDtosForAQuestion(List<Integer> questionIds)
            throws EvaluationException {
        if (questionIds.isEmpty()) {
            throw new EvaluationException("Missing questions.");
        }
        List<QuestionDimensionDto> qdDtoListForOneQuestion = new ArrayList();
        questionIds.stream()
                .forEach(q -> qdDtoListForOneQuestion.addAll(
                (List<QuestionDimensionDto>) questionDimensionDao.getAllQuestionDimensionsForQuestion(q).stream()
                        .map(qd -> questionDimensionMapper.toDto(qd))
                        .collect(Collectors.toList())
        ));
        return qdDtoListForOneQuestion;
    }

    private List<AnswerDto> provideAnswerDTOs(int filloutId) throws EvaluationException {
        List<AnswerDto> answerDTOs = new ArrayList<>();
        List<Answer> answers = answerDao.getAnswersForFillout(filloutId);
        if (answers.isEmpty()) {
            throw new EvaluationException("No answers for this fillout.");

        }
        answers.stream()
                .forEach(a -> answerDTOs.add(answerMapper.toDto(a)));
        return answerDTOs;
    }

    private List<DimensionDto> provideDimensionDTOs(int questionnaireId) throws EvaluationException {
        List<DimensionDto> dimensionDTOs = new ArrayList<>();
        List<Dimension> dimensions = dimensionDao.getAllDimensionsForQuestionnaire(questionnaireId);
        if (dimensions.isEmpty()) {
            throw new EvaluationException("No dimensions for this questionnaire.");

        }
        dimensions.stream()
                .forEach(a -> dimensionDTOs.add(dimensionMapper.toDto(a)));
        return dimensionDTOs;
    }

    private List<OptionDto> provideOptionDTOs(int questionnaireId) throws EvaluationException {
        List<OptionDto> optionDTOs = new ArrayList<>();
        List<Option> options = optionDao.getOptionsForQuestionnaire(questionnaireId);
        if (options.isEmpty()) {
            throw new EvaluationException("No options for this questionnaire.");

        }
        options.stream()
                .forEach(a -> optionDTOs.add(optionMapper.toDto(a)));
        return optionDTOs;
    }

    private Map<DimensionDto, Integer> calculateSumsForDimensions(List<AnswerDto> answerDTOs,
            List<OptionDto> optionDTOs, List<DimensionDto> dimensionDTOs,
            List<QuestionDimensionDto> qdDTOs) throws EvaluationException {
        Map<DimensionDto, Integer> sumMap = new HashMap<>();
        dimensionDTOs.stream()
                .forEach(i -> sumMap.put(i, 0));
        int questionId;
        int value;
        DimensionDto dimensionDTO = null;
        for (QuestionDimensionDto qdDTO : qdDTOs) {
            questionId = questionDimensionDao.getQuestionIdForQuestionDimension(qdDTO.getId())
                    .orElseThrow(() -> new EvaluationException("Missing questionId."));
            System.out.println("questionDimension-question: " + qdDTO.getId() + "-" + questionId);
            value = gatherValueForAnswer(answerDTOs, optionDTOs, questionId);
            if (value == -1) {
                throw new EvaluationException("Value for an option is missing.");
            }
            dimensionDTO = findDimensionDTOToQuestionDimensionDTO(dimensionDTOs, qdDTO)
                    .orElseThrow(() -> new EvaluationException("Missing dimension or questiondimension."));

            sumMap.put(dimensionDTO, value + sumMap.get(dimensionDTO));
        }
        return sumMap;
    }

    private int gatherValueForAnswer(List<AnswerDto> answerDTOs, List<OptionDto> optionDTOs, int questionId)
            throws EvaluationException {
        int value = -1;
        AnswerDto ans = answerDTOs.stream()
                .filter(a -> questionId == answerDao.getQuestionIdForAnswer(a.getId())
                .orElse(-1))
                .findAny()
                .orElseThrow(() -> new EvaluationException("Missing questionId."));
        final int optionId = answerDao.getOptionIdForAnswer(ans.getId())
                .orElseThrow(() -> new EvaluationException("Missing optionId."));
        OptionDto optionDto = optionDTOs.stream()
                .filter(o -> optionId == o.getId())
                .findAny()
                .orElseThrow(() -> new EvaluationException("Missing optionId."));
        value = optionDto.getValue();
        return value;
    }

    private Optional<DimensionDto> findDimensionDTOToQuestionDimensionDTO(List<DimensionDto> dimensionDTOs,
            QuestionDimensionDto qdDTO) throws EvaluationException {
        int dimensionIdForQuestionDimension = questionDimensionDao.getDimensionIdForQuestionDimension(qdDTO.getId())
                .orElseThrow(() -> new EvaluationException("Missing dimensionId."));
        return dimensionDTOs.stream()
                .filter(d -> dimensionIdForQuestionDimension == d.getId())
                .findAny();
    }

    private String createFullString(Map<DimensionDto, Integer> map, List<DimensionDto> dimensionDTOs) {
        String result = map.keySet().stream()
                .map(k -> findStringForDimension(k, map.get(k)))
                .collect(Collectors.joining());
        return result;
    }

    private String findStringForDimension(DimensionDto dimensionDTO, int value) {
        String result = null;
        if (value < dimensionDTO.getFirstBoundary()) {
            result = dimensionDTO.getFirstText();
        } else if (value < dimensionDTO.getSecondBoundary()) {
            result = dimensionDTO.getSecondText();
        } else if (value < dimensionDTO.getThirdBoundary()) {
            result = dimensionDTO.getThirdText();
        } else if (value < dimensionDTO.getFourthBoundary()) {
            result = dimensionDTO.getFourthText();
        } else if (value < dimensionDTO.getFifthBoundary()) {
            result = dimensionDTO.getFifthText();
        } else {
            result = dimensionDTO.getSixthText();
        }
        return result;
    }

}
