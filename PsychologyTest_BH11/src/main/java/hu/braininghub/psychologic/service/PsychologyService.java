/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.comparator.QuestionDtoComparator;
import hu.braininghub.psychologic.controller.CategoryDao;
import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.controller.QuestionDao;
import hu.braininghub.psychologic.controller.QuestionaireDao;
import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.mapper.CategoryMapper;
import hu.braininghub.psychologic.mapper.QuestionMapper;
import hu.braininghub.psychologic.mapper.QuestionaireMapper;
import hu.braininghub.psychologic.model.Category;
import hu.braininghub.psychologic.model.Question;
import hu.braininghub.psychologic.model.Questionaire;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Aliz
 */
@Slf4j
@Singleton
@Setter
public class PsychologyService {

    @Inject
    private CategoryMapper categoryMapper;

    @Inject
    private QuestionaireMapper questionaireMapper;

    @Inject
    private QuestionMapper questionMapper;

    @Inject
    private QuestionDtoComparator questionDtoComparator;

    @Inject
    private CategoryDao categoryDao;

    @Inject
    private QuestionaireDao questionaireDao;

    @Inject
    private QuestionDao questionDao;

    public boolean addCategory(CategoryDto categoryDTO) {
        try {
            Category category = categoryMapper.toEntity(categoryDTO);

            categoryDao.addCategory(category);
            log.info("New category added: " + categoryDTO);

        } catch (Exception e) {
            log.debug("Cannot create new category", e);
            return false;
        }
        return true;
    }
    
    public List<CategoryDto> getAllCategories() {
        try {
            return categoryDao.listAllCategories()
                    .stream()
                    .map(c -> categoryMapper.toDto(c))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.debug("Cannot list all categories", e);
            return new ArrayList<>();
        }
    }

    public List<String> getQuestionaireNames() {
        return questionaireDao.getAllQuestionaires().stream()
                .map(Questionaire::getName)
                .collect(Collectors.toList());
    }

    public boolean addQuestionaire(QuestionaireDto questionaireDTO) {
        try {
            Questionaire questionaire = questionaireMapper.toEntity(questionaireDTO);
            questionaireDao.addQuestionnaire(questionaire);
        } catch (Exception e) {
            log.debug("Cannot add questionaire", e);
            return false;
        }
        return true;
    }

    public List<QuestionaireDto> getAllQuestionaires() {
        return questionaireDao.getAllQuestionaires().stream()
                .map(q -> questionaireMapper.toDto(q))
                .collect(Collectors.toList());
    }

    public QuestionaireDto getQuestionnaireById(Integer id) {
        if (id == null) {
            return null;
        }
        QuestionaireDto questionaireDto = questionaireMapper.toDto(questionaireDao.getQuestionaireById(id));
        questionaireDto.setQuestionDto(
                questionaireDto.getQuestionDto()
                        .stream()
                        .sorted(questionDtoComparator)
                        .collect(Collectors.toList())
        );
        return questionaireDto;
    }

    public boolean addQuestion(QuestionDto questionDto) {
        try {
            Question question = questionMapper.toEntity(questionDto);
            questionDao.addQuestion(question);
        } catch (Exception e) {
            log.debug("Cannot add question", e);
            return false;
        }
        return true;
    }

    public QuestionDto getQuestionById(Integer id) {
        if (id == null) {
            return null;
        }
        return questionMapper.toDto(questionDao.getQuestionById(id));
    }
}
