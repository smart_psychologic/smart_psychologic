/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.controller.FilloutDao;
import hu.braininghub.psychologic.controller.QuestionaireDao;
import hu.braininghub.psychologic.controller.UserDao;
import javax.ejb.Singleton;
import javax.inject.Inject;

/**
 *
 * @author Marton Petrekanics
 */
@Singleton
public class Evaluation2Service {

    @Inject
    private QuestionaireDao questionaireDao;

    @Inject
    private FilloutDao filloutDao;

    @Inject
    private UserDao userDao;

    public int findFilloutForUserName(String name) throws EvaluationException {
        int userId = userDao.findByName(name)
                .orElseThrow(() -> new EvaluationException("Missing userId."));
        int filloutId = filloutDao.findByUserId(userId)
                .orElseThrow(() -> new EvaluationException("Missing userId."));
        return filloutId;
    }

    public String findQuestionnaireNameForFillout(int filloutId) throws EvaluationException {
        int questionnaireId = filloutDao.getQuestionnaireIdForFillout(filloutId)
                .orElseThrow(() -> new EvaluationException("Missing questionnaireId."));
        String questionnaireName = questionaireDao.getQuestionaireById(questionnaireId).getName();
        return questionnaireName;
    }
}
