/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

/**
 *
 * @author Marton Petrekanics
 */
public class EvaluationException extends Exception {

    /**
     *
     * @exception when there is no sufficient data to evaluate a fillout
     *
     */
    public EvaluationException(String msg) {
        super(msg);
    }

    public EvaluationException(String msg, Throwable cause) {
        super(msg, cause);
    }

}
