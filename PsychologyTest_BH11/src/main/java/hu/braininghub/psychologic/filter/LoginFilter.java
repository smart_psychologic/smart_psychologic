/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.filter;

import hu.braininghub.psychologic.service.GroupService;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Aliz
 */
@WebFilter(urlPatterns = "/*")
public class LoginFilter extends HttpFilter {

    @Inject
    private GroupService service;

    //every jsp should have these parameters
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String username = req.getRemoteUser();
        List<String> groups = service.findGroups(username);
        String groupName = groups.stream().findFirst().orElse(null);
        if (username != null) {
            req.setAttribute("username", username);
            req.setAttribute("groupName", groupName);
        }

        chain.doFilter(req, res);
    }

}
