/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Marton Petrekanics
 */
@Entity
@Table(name = "options")
@ToString
@Setter
public class Option implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "questionaire_id")
    @Getter
    private Questionaire questionnaire;

    @Column(name = "`rank`")
    @Getter
    private Integer rank;

    @Column(name = "text")
    @Getter
    private String text;

    @Column(name = "value")
    @Getter
    private Integer value;

    @OneToMany(mappedBy = "option")
    @Getter
    private Set<Answer> answers;
}
