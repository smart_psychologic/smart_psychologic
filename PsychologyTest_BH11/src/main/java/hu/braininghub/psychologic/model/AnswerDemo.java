/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Aliz
 */
//FIXME: delete after demo
@Entity
@Table(name = "answers_demo")
@ToString
public class AnswerDemo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Setter
    @Getter
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "question_id", nullable = false)
    @Setter
    @Getter
    private Question question;

    @Setter
    @Getter
    @Enumerated(EnumType.STRING)
    @Column(name = "answer", nullable = false)
    private OptionDemo option;

}
