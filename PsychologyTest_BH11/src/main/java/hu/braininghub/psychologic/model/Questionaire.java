/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Questionaire
 *
 * @author Aliz
 */
@Entity
@Table(name = "questionaire")
@NoArgsConstructor
@Setter
public class Questionaire implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", nullable = false)
    @Getter
    private Category category;

    @Column(name = "name", length = 45, nullable = false, unique = true)
    @Getter
    private String name;

    @Column(name = "short_description", length = 45)
    @Getter
    private String shortDescription;

    @Column(name = "long_description", length = 6000)
    @Getter
    @Lob
    private String longDescription;

    @Column(name = "extra_questions")
    @Getter
    private String extraQuestions;

    @OneToMany(mappedBy = "questionnaire")
    @Getter
    private Set<Option> options;

    @OneToMany(mappedBy = "questionnaire")
    @Getter
    private Set<Dimension> dimensions;

    @OneToMany(mappedBy = "questionnaire")
    @Getter
    private Set<Question> questions;

    @OneToMany(mappedBy = "questionnaire")
    @Getter
    private Set<Fillout> fillouts;
}
