/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Category
 *
 * @author Aliz
 */
@NoArgsConstructor
@NamedQueries(
        @NamedQuery(name = "Category.findAll", query = "SELECT c FROM Category c")
)
@Entity
@Table(name = "categories")
@Setter
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    private Integer id;

    @Column(name = "name", length = 45, nullable = false, unique = true)
    @Getter
    private String name;

    @OneToMany(mappedBy = "category", fetch = FetchType.EAGER)
    @Getter
    private Set<Questionaire> questionaires;

    public Category(String name) {
        this.name = name;
    }
}
