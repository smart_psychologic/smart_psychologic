/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Entity
@Table(name = "dimensions")
@Setter
@Getter
public class Dimension implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "questionaire_id")
    private Questionaire questionnaire;

    @Column(name = "first_boundary")
    private Integer firstBoundary;

    @Column(name = "second_boundary")
    private Integer secondBoundary;

    @Column(name = "third_boundary")
    private Integer thirdBoundary;

    @Column(name = "fourth_boundary")
    private Integer fourthBoundary;

    @Column(name = "fifth_boundary")
    private Integer fifthBoundary;

    @Column(name = "first_text")
    private String firstText;

    @Column(name = "second_text")
    private String secondText;

    @Column(name = "third_text")
    private String thirdText;

    @Column(name = "fourth_text")
    private String fourthText;

    @Column(name = "fifth_text")
    private String fifthText;

    @Column(name = "sixth_text")
    private String sixthText;

    @OneToMany(mappedBy = "dimension")
    private Set<QuestionDimension> questionDimensions;

}
