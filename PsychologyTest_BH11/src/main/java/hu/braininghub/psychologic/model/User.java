/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author mito_qa
 */
@Entity
@Table(name = "user")
@Setter
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    private Integer id;

    @Column(name = "name", length = 100, unique = true, nullable = false)
    @Getter
    private String name;

    @Column(name = "first_name")
    @Getter
    private String firstName;

    @Column(name = "last_name")
    @Getter
    private String lastName;

    @Column(name = "email_address")
    @Getter
    private String emailAddress;

    @Column(name = "date_of_birth")
    @Getter
    private Date dateOfBirth;

    @Column(name = "password")
    @Getter
    private String password;

    @Column(name = "number_of_filledout")
    @Getter
    private Integer numberOfFilledout;

    @OneToMany(mappedBy = "user")
    @Getter
    private Set<Fillout> fillouts;

}
