/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Entity
@Table(name = "questions")
@Setter
@Getter
public class Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "questionaire_id")
    private Questionaire questionnaire;

    @Column(name = "text")
    private String text;

    @Column(name = "`rank`")
    private Integer rank;

    @Column(name = "media")
    private String media;

    @Column(name = "media_type")
    private String mediaType;

    @Column(name = "alternative_next")
    private Integer alternativeNext;

    @OneToMany(mappedBy = "question")
    private Set<Answer> answers;

    //FIXME: delete after demo
    @OneToOne(mappedBy = "question")
    private AnswerDemo answerDemo;

    @OneToMany(mappedBy = "question")
    private Set<QuestionDimension> questionDimensions;

}
