/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.model;

import lombok.Getter;

/**
 *
 * @author Aliz
 */
public enum OptionDemo {

    NOT_AT_ALL("Egyáltalán nem"),
    LITTLE("Kicsit"),
    AVERAGE("Közepesen"),
    ENOUGH("Eléggé"),
    VERY_MUCH("Nagyon");

    @Getter
    private final String text;

    private OptionDemo(String text) {
        this.text = text;
    }

}
