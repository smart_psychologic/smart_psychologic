/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author Marton Petrekanics
 */
@NoArgsConstructor
@ToString
@Setter
public class AnswerDto implements Serializable {

    @Getter
    private Integer id;

    @Getter
    private FilloutDto filloutDto;

    @Getter
    private QuestionDto questionDto;

    @Getter
    private OptionDto optionDto;
}
