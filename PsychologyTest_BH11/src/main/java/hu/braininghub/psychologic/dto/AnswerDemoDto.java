/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.dto;

import hu.braininghub.psychologic.model.OptionDemo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Aliz
 */
@NoArgsConstructor
public class AnswerDemoDto {

    @Setter
    @Getter
    private Integer id;

    @Setter
    @Getter
    private QuestionDto questionDto;

    @Setter
    @Getter
    private OptionDemo option;

}
