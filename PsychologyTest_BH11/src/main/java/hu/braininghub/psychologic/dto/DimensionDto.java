/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@NoArgsConstructor
@Setter
@Getter
public class DimensionDto implements Serializable {

    private Integer id;

    private String name;

    private QuestionaireDto questionnaireDto;

    private Integer firstBoundary;

    private Integer secondBoundary;

    private Integer thirdBoundary;

    private Integer fourthBoundary;

    private Integer fifthBoundary;

    private String firstText;

    private String secondText;

    private String thirdText;

    private String fourthText;

    private String fifthText;

    private String sixthText;
}
