/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@NoArgsConstructor
@Setter
public class FilloutDto implements Serializable {

    @Getter
    private Integer id;

    @Getter
    private UserDto userDto;

    @Getter
    private QuestionaireDto questionnaireDto;

    @Getter
    private Date filloutDate;

    @Getter
    private Double filloutDuration;

}
