/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@NoArgsConstructor
@Setter
public class UserDto implements Serializable {

    @Getter
    private Integer id;

    @Getter
    private String name;

    @Getter
    private String firstName;

    @Getter
    private String lastName;

    @Getter
    private String emailAddress;

    @Getter
    private Date dateOfBirth;

    @Getter
    private String password;

    @Getter
    private Integer numberOfFilledout;

}
