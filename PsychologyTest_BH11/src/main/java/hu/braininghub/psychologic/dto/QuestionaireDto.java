/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.dto;

import java.io.Serializable;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author mito_qa
 */
@NoArgsConstructor
@Setter
public class QuestionaireDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Getter
    private Integer id;

    @Getter
    private String name;

    @Getter
    private String shortDescription;

    @Getter
    private String longDescription;

    @Getter
    private String extraQuestions;

    @Getter
    private CategoryDto categoryDTO;

    @Getter
    private List<QuestionDto> questionDto;
}
