/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.AnswerDto;
import hu.braininghub.psychologic.model.Answer;
import java.io.Serializable;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Singleton
@LocalBean
@Setter
public class AnswerMapper extends AbstractPsychologicMapper implements Mapper<Answer, AnswerDto>, Serializable {

    private static final long serialVersionUID = 1L;

    @Override
    public Answer toEntity(AnswerDto dto) {
        Answer answer = new Answer();
        useBeanUtils(answer, dto);

        return answer;
    }

    @Override
    public AnswerDto toDto(Answer entity) {
        if (entity == null) {

            return null;
        }

        AnswerDto dto = new AnswerDto();
        useBeanUtils(dto, entity);

        return dto;
    }
}
