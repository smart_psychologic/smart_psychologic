/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.FilloutDto;
import hu.braininghub.psychologic.model.Fillout;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Singleton
@LocalBean
@Setter
public class FilloutMapper extends AbstractPsychologicMapper implements Mapper<Fillout, FilloutDto> {

    private static final long serialVersionUID = 1L;

    @Override
    public Fillout toEntity(FilloutDto dto) {
        Fillout fillout = new Fillout();

        useBeanUtils(fillout, dto);

        return fillout;
    }

    @Override

    public FilloutDto toDto(Fillout entity) {
        if (entity == null) {

            return null;
        }

        FilloutDto dto = new FilloutDto();
        useBeanUtils(dto, entity);
        return dto;
    }
}
