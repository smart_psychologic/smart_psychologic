/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.FeedbackDto;
import hu.braininghub.psychologic.model.Feedback;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author mito_qa
 */
@Singleton
@LocalBean
@Setter
public class FeedbackMapper extends AbstractPsychologicMapper implements Mapper<Feedback, FeedbackDto> {

    @Override
    public Feedback toEntity(FeedbackDto dto) {
        Feedback feedback = new Feedback();
        
        useBeanUtils(feedback, dto);
        
        return feedback;
    }

    @Override
    public FeedbackDto toDto(Feedback entity) {
        if (entity == null) {

            return null;
        }
        
        FeedbackDto dto = new FeedbackDto();
        useBeanUtils(dto, entity);
        
        return dto;
    }
    
}
