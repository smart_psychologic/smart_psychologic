/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.AnswerDemoDto;
import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.model.AnswerDemo;
import hu.braininghub.psychologic.model.Question;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author Aliz
 */
@Singleton
@LocalBean
@Slf4j
public class AnswerDemoMapper implements Mapper<AnswerDemo, AnswerDemoDto>, Serializable {

    @Inject
    private QuestionMapper questionMapper;

    private static final long serialVersionUID = 1L;

    @Override
    public AnswerDemo toEntity(AnswerDemoDto dto) {
        AnswerDemo answerDemo = new AnswerDemo();
        try {
            BeanUtils.copyProperties(answerDemo, dto);    //destination, source
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("Cannot convert DTO to Entity", ex);
        }

        Question question = new Question();
        question.setId(dto.getQuestionDto().getId());
        answerDemo.setQuestion(question);

        return answerDemo;
    }

    @Override
    public AnswerDemoDto toDto(AnswerDemo entity) {
        if (entity == null) {
            return null;
        }
        AnswerDemoDto dto = new AnswerDemoDto();
        try {
            BeanUtils.copyProperties(dto, entity);    //destination, source
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.error("Cannot convert Entity to DTO", ex);
        }

        QuestionDto questionDto = questionMapper.toDto(entity.getQuestion());
        dto.setQuestionDto(questionDto);

        return dto;
    }

}
