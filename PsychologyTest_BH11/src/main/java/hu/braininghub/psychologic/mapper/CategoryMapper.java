/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.model.Category;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author Aliz
 */
@Singleton
@LocalBean
@Setter
public class CategoryMapper extends AbstractPsychologicMapper implements Mapper<Category, CategoryDto> {

    private static final long serialVersionUID = 1L;

    @Override
    public Category toEntity(CategoryDto dto) {
        Category category = new Category();

        useBeanUtils(category, dto);

        return category;
    }

    @Override
    public CategoryDto toDto(Category entity) {
        if (entity == null) {
            return null;
        }

        CategoryDto categoryDTO = new CategoryDto();
        useBeanUtils(categoryDTO, entity);

        return categoryDTO;
    }
}
