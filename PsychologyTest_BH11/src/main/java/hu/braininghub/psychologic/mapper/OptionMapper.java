/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.OptionDto;
import hu.braininghub.psychologic.model.Option;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Singleton
@LocalBean
@Setter
public class OptionMapper extends AbstractPsychologicMapper implements Mapper<Option, OptionDto> {

    private static final long serialVersionUID = 1L;

    @Override
    public Option toEntity(OptionDto dto) {
        Option option = new Option();

        useBeanUtils(option, dto);

        return option;
    }

    @Override
    public OptionDto toDto(Option entity) {
        if (entity == null) {

            return null;
        }

        OptionDto optionDto = new OptionDto();
        useBeanUtils(optionDto, entity);
        return optionDto;
    }
}
