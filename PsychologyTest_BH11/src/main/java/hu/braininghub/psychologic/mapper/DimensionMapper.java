/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.DimensionDto;
import hu.braininghub.psychologic.model.Dimension;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Singleton
@LocalBean
@Setter
public class DimensionMapper extends AbstractPsychologicMapper implements Mapper<Dimension, DimensionDto> {

    private static final long serialVersionUID = 1L;

    @Override
    public Dimension toEntity(DimensionDto dto) {
        Dimension dimension = new Dimension();

        useBeanUtils(dimension, dto);

        return dimension;
    }

    @Override
    public DimensionDto toDto(Dimension entity) {
        if (entity == null) {

            return null;
        }

        DimensionDto dto = new DimensionDto();
        useBeanUtils(dto, entity);

        return dto;
    }
}
