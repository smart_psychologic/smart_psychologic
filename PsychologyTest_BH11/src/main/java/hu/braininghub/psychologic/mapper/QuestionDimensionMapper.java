/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.QuestionDimensionDto;
import hu.braininghub.psychologic.model.QuestionDimension;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Singleton
@LocalBean
@Setter
public class QuestionDimensionMapper extends AbstractPsychologicMapper implements Mapper<QuestionDimension, QuestionDimensionDto> {

    private static final long serialVersionUID = 1L;

    @Override
    public QuestionDimension toEntity(QuestionDimensionDto dto) {
        QuestionDimension entity = new QuestionDimension();

        useBeanUtils(entity, dto);
        return entity;
    }

    @Override
    public QuestionDimensionDto toDto(QuestionDimension entity) {
        if (entity == null) {

            return null;
        }
        QuestionDimensionDto dto = new QuestionDimensionDto();
        useBeanUtils(dto, entity);
        return dto;
    }
}
