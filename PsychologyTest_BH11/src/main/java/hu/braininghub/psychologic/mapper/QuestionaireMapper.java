/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.model.Category;
import hu.braininghub.psychologic.model.Question;
import hu.braininghub.psychologic.model.Questionaire;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.AccessLevel;
import lombok.Setter;

/**
 *
 * @author Aliz
 */
@Singleton
@LocalBean
@Setter(AccessLevel.PACKAGE)
public class QuestionaireMapper extends AbstractPsychologicMapper implements Mapper<Questionaire, QuestionaireDto> {

    private static final long serialVersionUID = 1L;

    @Inject
    private CategoryMapper categoryMapper;

    @Inject
    private QuestionMapper questionMapper;

    @Override
    public Questionaire toEntity(QuestionaireDto dto) {
        Questionaire questionaire = new Questionaire();

        useBeanUtils(questionaire, dto);

        Category category = new Category();
        category.setId(dto.getCategoryDTO().getId());
        questionaire.setCategory(category);

        return questionaire;
    }

    @Override
    public QuestionaireDto toDto(Questionaire entity) {
        return toDto(entity, true);
    }

    public QuestionaireDto toDto(Questionaire entity, boolean withSub) {
        if (entity == null) {
            return null;
        }
        QuestionaireDto questionaireDTO = new QuestionaireDto();

        useBeanUtils(questionaireDTO, entity);

        if (withSub) { //prevent recursion
            CategoryDto categoryDTO = categoryMapper.toDto(entity.getCategory());
            questionaireDTO.setCategoryDTO(categoryDTO);
        }

        if (withSub) { //prevent recursion
            List<QuestionDto> questionDtos = new ArrayList<>();
            for (Question question : entity.getQuestions()) {
                questionDtos.add(questionMapper.toDto(question));
            }
            questionaireDTO.setQuestionDto(questionDtos);
        }

        return questionaireDTO;
    }
}
