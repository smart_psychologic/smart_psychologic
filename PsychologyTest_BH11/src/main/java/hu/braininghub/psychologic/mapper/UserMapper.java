/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.UserDto;
import hu.braininghub.psychologic.model.User;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import lombok.Setter;

/**
 *
 * @author Marton Petrekanics
 */
@Singleton
@LocalBean
@Setter
public class UserMapper extends AbstractPsychologicMapper implements Mapper<User, UserDto> {

    private static final long serialVersionUID = 1L;

    @Override
    public User toEntity(UserDto dto) {
        User entity = new User();

        useBeanUtils(entity, dto);

        return entity;
    }

    @Override
    public UserDto toDto(User entity) {
        if (entity == null) {

            return null;
        }

        UserDto dto = new UserDto();
        useBeanUtils(dto, entity);

        return dto;
    }
}
