/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.QuestionDto;
import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.model.Question;
import hu.braininghub.psychologic.model.Questionaire;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.inject.Inject;
import lombok.Setter;

/**
 *
 * @author Aliz
 */
@Singleton
@LocalBean
@Setter
public class QuestionMapper extends AbstractPsychologicMapper implements Mapper<Question, QuestionDto> {

    private static final long serialVersionUID = 1L;

    @Inject
    private QuestionaireMapper questionaireMapper;

    @Override
    public Question toEntity(QuestionDto dto) {
        Question question = new Question();

        useBeanUtils(question, dto);

        Questionaire questionaire = new Questionaire();
        questionaire.setId(dto.getQuestionnaireDto().getId());
        question.setQuestionnaire(questionaire);

        return question;
    }

    @Override
    public QuestionDto toDto(Question entity) {
        if (entity == null) {
            return null;
        }

        QuestionDto questionDto = new QuestionDto();
        useBeanUtils(questionDto, entity);

        QuestionaireDto questionaireDTO = questionaireMapper.toDto(entity.getQuestionnaire(), false);
        questionDto.setQuestionnaireDto(questionaireDTO);

        return questionDto;
    }

}
