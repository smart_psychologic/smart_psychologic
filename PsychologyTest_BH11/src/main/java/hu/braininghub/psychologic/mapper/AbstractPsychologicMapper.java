/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import java.lang.reflect.InvocationTargetException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;

/**
 *
 * @author Marton Petrekanics
 */
@Slf4j
public class AbstractPsychologicMapper {

    public void useBeanUtils(Object to, Object from) {
        try {
            BeanUtils.copyProperties(to, from);    //destination, source
        } catch (IllegalAccessException | InvocationTargetException ex) {
            log.debug("Cannot convert between DTO and Entity", ex);
        }
    }

}
