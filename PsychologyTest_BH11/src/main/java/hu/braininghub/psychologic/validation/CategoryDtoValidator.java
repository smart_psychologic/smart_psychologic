/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.validation;

import hu.braininghub.psychologic.dto.CategoryDto;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class CategoryDtoValidator implements Validator<CategoryDto> {

    @Override
    public void isValid(CategoryDto t) throws ValidationException {
        Objects.requireNonNull(t);

        if (t.getName() == null) {
            throw new ValidationException("A kategória neve üres.");
        }

        if (t.getName().trim().isEmpty()) {
            throw new ValidationException("A kategória neve üres.");
        }  //isBlank() in Java 11

    }

}
