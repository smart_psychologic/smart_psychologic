/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.validation;

import hu.braininghub.psychologic.dto.UserDto;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class UserDtoValidator implements Validator<UserDto> {

    @Override
    public void isValid(UserDto t) throws ValidationException {
        Objects.requireNonNull(t);

        if (t.getName() == null) {
            throw new ValidationException("A felhasználónév nem lehet üres.");
        }

        if (t.getName().trim().isEmpty()) {
            throw new ValidationException("A felhasználónév nem lehet üres.");
        }

        if (t.getPassword() == null) {
            throw new ValidationException("A jelszó nem lehet üres.");
        }

        if (t.getPassword().length() < 5) {
            throw new ValidationException("Kérem adjon meg minimum 5 karakterből álló jelszót.");
        }
    }

}
