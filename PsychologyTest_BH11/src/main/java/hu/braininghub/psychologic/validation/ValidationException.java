/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.validation;

/**
 * Custom Validation Exception
 *
 * @author Aliz
 */
public class ValidationException extends Exception {

    /**
     *
     * @param msg about what was wrong
     */
    public ValidationException(String msg) {
        super(msg);
    }
}
