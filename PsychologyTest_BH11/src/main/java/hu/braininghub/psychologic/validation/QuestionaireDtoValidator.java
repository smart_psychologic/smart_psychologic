/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.validation;

import hu.braininghub.psychologic.dto.QuestionaireDto;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class QuestionaireDtoValidator implements Validator<QuestionaireDto> {

    @Override
    public void isValid(QuestionaireDto t) throws ValidationException {
        Objects.requireNonNull(t);

        if (t.getName() == null) {
            throw new ValidationException("A kérdőív neve üres.");
        }

        if (t.getName().trim().isEmpty()) {
            throw new ValidationException("A kérdőív neve üres.");
        }

        if (t.getCategoryDTO() == null) {
            throw new ValidationException("A kategória nem létezik, kérem először hozzon létre kategóriát.");
        }

    }
}
