/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.validation;

/**
 * Validator for T
 *
 * @author Aliz
 */
public interface Validator<T> {

    /**
     * Checking if object is valid
     *
     * @param t T type for validation
     * @throws ValidationException if the t object is invalid (contains message
     * about it)
     * @throws NullPointerException if the t is null
     */
    public void isValid(T t) throws ValidationException;
}
