/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.validation;

import hu.braininghub.psychologic.dto.FeedbackDto;
import java.util.Objects;

/**
 *
 * @author mito_qa
 */
public class FeedbackDtoValidator implements Validator<FeedbackDto> {

    @Override
    public void isValid(FeedbackDto t) throws ValidationException {
        Objects.requireNonNull(t);
        
        if (t.getFirstName()== null) {
            throw new ValidationException("A vezetéknév nem lehet üres.");
        }
        
        if (t.getFirstName().trim().isEmpty()) {
            throw new ValidationException("A vezetéknév nem lehet üres.");
        }
        
        if (t.getLastName()== null) {
            throw new ValidationException("A keresztnév nem lehet üres.");
        }
        
        if (t.getLastName().trim().isEmpty()) {
            throw new ValidationException("A keresztnév nem lehet üres.");
        }
        
        if (t.getMessage()== null) {
            throw new ValidationException("Az üzenet nem lehet üres.");
        }
        
        if (t.getMessage().trim().isEmpty()) {
            throw new ValidationException("Az üzenet nem lehet üres.");
        }
    }
    
}
