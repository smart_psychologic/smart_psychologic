/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.validation;

import hu.braininghub.psychologic.dto.QuestionDto;
import java.util.Objects;

/**
 *
 * @author Aliz
 */
public class QuestionDtoValidator implements Validator<QuestionDto> {

    @Override
    public void isValid(QuestionDto t) throws ValidationException {
        Objects.requireNonNull(t);

        if (t.getText() == null) {
            throw new ValidationException("A kérdés szövege nem lehet üres.");
        }

        if (t.getText().trim().isEmpty()) {
            throw new ValidationException("A kérdés szövege nem lehet üres.");
        }

        if (t.getQuestionnaireDto() == null) {
            throw new ValidationException("A kérdőív nem létezik, kérem először hozzon létre kérdőívet.");
        }

        if (t.getRank() == null) {
            throw new ValidationException("Kérem adja meg a kérdés sorszámát.");
        }
    }

}
