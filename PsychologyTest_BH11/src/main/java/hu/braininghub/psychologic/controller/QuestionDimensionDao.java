/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.QuestionDimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
public class QuestionDimensionDao {

    @PersistenceContext
    private EntityManager manager;

    public Optional<Integer> getDimensionIdForQuestionDimension(Integer id) {

        QuestionDimension questionDimension = (QuestionDimension) manager.createQuery("SELECT q FROM QuestionDimension q WHERE q.id = :id")
                .setParameter("id", id)
                .getSingleResult();
        return Optional.of(questionDimension.getDimension().getId());

    }

    public Optional<Integer> getQuestionIdForQuestionDimension(Integer id) {

        QuestionDimension questionDimension = (QuestionDimension) manager.createQuery("SELECT q FROM QuestionDimension q WHERE q.id = :id")
                .setParameter("id", id)
                .getSingleResult();

        return Optional.of(questionDimension.getQuestion().getId());

    }

    public List<QuestionDimension> getAllQuestionDimensionsForQuestion(Integer id) {
        try {
            List<QuestionDimension> questionDimensionList
                    = (List<QuestionDimension>) manager.createQuery("SELECT q FROM QuestionDimension q WHERE q.question.id = :id")
                            .setParameter("id", id)
                            .getResultList();
            return questionDimensionList;

        } catch (NoResultException e) {
            return new ArrayList();
        }
    }
}
