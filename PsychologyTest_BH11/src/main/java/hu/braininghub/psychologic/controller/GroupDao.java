/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import java.util.List;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Aliz
 */
@Singleton
public class GroupDao {

    @PersistenceContext
    private EntityManager manager;

    public List<String> getGroups(String username) {
        return manager.createQuery("SELECT g.groupName FROM UserGroup g WHERE g.name = :name").setParameter("name", username)
                .getResultList();
    }
}
