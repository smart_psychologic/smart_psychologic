/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Category;
import hu.braininghub.psychologic.model.Questionaire;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
public class QuestionaireDao {

    @PersistenceContext
    private EntityManager manager;

    @Inject
    private CategoryDao categoryDao;

    public void addQuestionnaire(Questionaire questionaire) {
        Category category = categoryDao.getCategory(questionaire.getCategory().getId());
        questionaire.setCategory(category); //requires persisted category
        manager.persist(questionaire);
        manager.flush();
    }

    public List<Questionaire> getAllQuestionaires() {
        return manager.createQuery("SELECT q FROM Questionaire q")
                .getResultList();
    }

    public Questionaire getQuestionaireById(Integer id) {
        return manager.find(Questionaire.class, id);
    }
}
