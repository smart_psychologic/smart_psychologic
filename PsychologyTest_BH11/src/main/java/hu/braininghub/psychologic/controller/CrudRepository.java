/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import java.util.Collection;
import java.util.Optional;

/**
 *
 * @author mito_qa
 */
public interface CrudRepository<Entity, ID> {

    Collection<Entity> findAll();

    void deleteById(ID id);

    void save(Entity entity);

    void update(Entity entity);

    Optional<Entity> findById(ID id);

    int count();
}
