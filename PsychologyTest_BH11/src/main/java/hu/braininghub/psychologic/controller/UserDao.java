/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.User;
import hu.braininghub.psychologic.model.UserGroup;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Aliz
 */
@Stateless
public class UserDao {

    @PersistenceContext
    private EntityManager manager;

    public void addUser(User user) {
        manager.persist(user);
        UserGroup userGroup = new UserGroup();
        userGroup.setGroupName("user"); //adding to userGroup
        userGroup.setName(user.getName());
        manager.persist(userGroup);
        manager.flush();
    }

    public Optional<Integer> findByName(String name) {
        try {
            User user = (User) manager.createQuery("SELECT f FROM User f WHERE f.name= :name")
                    .setParameter("name", name)
                    .getSingleResult();

            return Optional.of(user.getId());

        } catch (NoResultException e) {
            return Optional.empty();
        }
    }
}
