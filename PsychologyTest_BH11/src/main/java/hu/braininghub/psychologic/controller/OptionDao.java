/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Option;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
public class OptionDao {

    @PersistenceContext
    private EntityManager manager;

    public List<Option> getOptionsForQuestionnaire(int id) {
        try {

            List<Option> optionList
                    = (List<Option>) manager.createQuery("SELECT o FROM Option o WHERE o.questionnaire.id = :id")
                            .setParameter("id", id)
                            .getResultList();
            return optionList;

        } catch (NoResultException e) {
            return new ArrayList();
        }
    }
}
