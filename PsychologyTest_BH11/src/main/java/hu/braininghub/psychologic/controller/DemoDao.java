/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.AnswerDemo;
import hu.braininghub.psychologic.model.Question;
import hu.braininghub.psychologic.model.Questionaire;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Aliz
 */
@Stateless
public class DemoDao {

    @Inject
    private QuestionaireDao questionaireDao;

    @Inject
    private QuestionDao questionDao;

    @PersistenceContext
    private EntityManager manager;

    public Optional<Question> getNextQuestionDemo(Integer questionaireId) {
        Questionaire questionaire = questionaireDao.getQuestionaireById(questionaireId);
        if (questionaire == null) {   //not found
            return Optional.empty();
        }
        return questionaire.getQuestions().stream()
                .filter(question -> question.getAnswerDemo() == null) //those questions which doesn-t have answers yet
                .sorted((a, b) -> a.getRank() - b.getRank()) //sorted by rank
                .findFirst();
    }

    public List<AnswerDemo> getAllAnswerDemo(Integer questionaireId) {
        Questionaire questionaire = questionaireDao.getQuestionaireById(questionaireId);
        if (questionaire == null) {   //not found
            return new ArrayList<>();
        }
        return questionaire.getQuestions().stream()
                .filter(question -> question.getAnswerDemo() != null)
                .sorted((a, b) -> a.getRank() - b.getRank())
                .map(question -> question.getAnswerDemo())
                .collect(Collectors.toList());
    }

    public void addAnswerDemo(AnswerDemo answerDemo) {
        Question question = questionDao.getQuestionById(answerDemo.getQuestion().getId());
        answerDemo.setQuestion(question);
        manager.persist(answerDemo);
        manager.flush();
    }
}
