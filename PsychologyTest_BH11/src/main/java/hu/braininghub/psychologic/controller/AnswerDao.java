/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Answer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
public class AnswerDao {

    @PersistenceContext
    private EntityManager manager;

    public List<Answer> getAnswersForFillout(int id) {
        try {
            List<Answer> answerList = (List<Answer>) manager.createQuery("SELECT a FROM Answer a WHERE a.fillout.id = :id")
                    .setParameter("id", id)
                    .getResultList();
            return answerList;

        } catch (NoResultException e) {
            return new ArrayList();
        }

    }

    public Optional<Integer> getQuestionIdForAnswer(Integer id) {

        Answer answer = (Answer) manager.createQuery("SELECT a FROM Answer a WHERE a.id = :id")
                .setParameter("id", id)
                .getSingleResult();
        return Optional.of(answer.getQuestion().getId());

    }

    public Optional<Integer> getOptionIdForAnswer(Integer id) {

        Answer a = (Answer) manager.createQuery("SELECT a FROM Answer a WHERE a.id = :id")
                .setParameter("id", id)
                .getSingleResult();
        return Optional.of(a.getOption().getId());

    }

}
