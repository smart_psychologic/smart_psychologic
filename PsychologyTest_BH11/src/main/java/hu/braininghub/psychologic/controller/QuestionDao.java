/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Question;
import hu.braininghub.psychologic.model.Questionaire;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
public class QuestionDao {

    @PersistenceContext
    private EntityManager manager;

    @Inject
    private QuestionaireDao questionaireDao;

    public void addQuestion(Question question) {
        Questionaire questionaire = questionaireDao.getQuestionaireById(question.getQuestionnaire().getId());
        question.setQuestionnaire(questionaire);
        manager.persist(question);
        manager.flush();
    }

    public List<Integer> getAllQuestionIdsForQuestionnaire(int id) {
        try {
            List<Question> questionList = (List<Question>) manager.createQuery("SELECT q FROM Question q WHERE q.questionnaire.id = :id")
                    .setParameter("id", id)
                    .getResultList();

            return questionList.stream().map(qq -> qq.getId()).collect(Collectors.toList());

        } catch (NoResultException e) {
            return new ArrayList();
        }

    }

    public Question getQuestionById(Integer id) {
        return manager.find(Question.class, id);
    }
}
