/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Dimension;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
public class DimensionDao {

    @PersistenceContext
    private EntityManager manager;

    public List<Dimension> getAllDimensionsForQuestionnaire(int id) {
        try {
            List<Dimension> dimensionList
                    = (List<Dimension>) manager.createQuery("SELECT d FROM Dimension d WHERE d.questionnaire.id = :id")
                            .setParameter("id", id)
                            .getResultList();
            return dimensionList;

        } catch (NoResultException e) {
            return new ArrayList();
        }
    }
}
