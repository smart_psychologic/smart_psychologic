/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Feedback;
import java.util.Collection;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
@LocalBean
public class FeedbackDao implements CrudRepository<Feedback, Integer> {

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Collection<Feedback> findAll() {
        return manager.createQuery("SELECT f FROM Feedback f")
                .getResultList();
    }

    @Override
    public void deleteById(Integer id) {
        Feedback f = manager.find(Feedback.class, id);

        if (f != null) {
            manager.remove(f);
        }

    }

    @Override
    public void save(Feedback entity) {
        manager.persist(entity);
        manager.flush();
    }

    @Override
    public void update(Feedback entity) {
        manager.merge(entity);
    }

    @Override
    public Optional<Feedback> findById(Integer id) {
        try {
            Feedback f = (Feedback) manager.createQuery("SELECT f FROM Feedback f WHERE f.id= :id")
                    .setParameter("id", id)
                    .getSingleResult();

            return Optional.of(f);

        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public int count() {
        return manager.createQuery("SELECT f FROM Feedback f")
                .getResultList().size();
    }
}
    

