/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Category;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
@LocalBean
public class CategoryDao implements CrudRepository<Category, Integer> {

    @PersistenceContext
    private EntityManager manager;

    public void addCategory(Category category) {
        manager.persist(category);
        manager.flush();
    }

    public List<Category> listAllCategories() {
        return manager.createNamedQuery("Category.findAll", Category.class).getResultList();
    }

    public Category getCategory(Integer id) {
        return manager.find(Category.class, id);
    }

    @Override
    public Collection<Category> findAll() {
        return manager.createQuery("SELECT c FROM Category c")
                .getResultList();
    }

    @Override
    public void deleteById(Integer id) {
        Category category = manager.find(Category.class, id);

        if (category != null) {
            manager.remove(category);
        }
    }

    @Override
    public void save(Category entity) {
        manager.persist(entity);
    }

    @Override
    public void update(Category entity) {
        manager.merge(entity);
    }

    @Override
    public Optional<Category> findById(Integer id) {
        try {
            Category category = (Category) manager.createQuery("SELECT c FROM Category c WHERE c.id= :id")
                    .setParameter("id", id)
                    .getSingleResult();

            return Optional.of(category);

        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public int count() {
        return manager.createQuery("SELECT c FROM Category c")
                .getResultList().size();
    }

}
