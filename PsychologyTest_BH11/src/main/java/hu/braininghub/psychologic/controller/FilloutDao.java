/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.controller;

import hu.braininghub.psychologic.model.Fillout;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mito_qa
 */
@Stateless
@LocalBean
public class FilloutDao implements CrudRepository<Fillout, Integer> {

    @PersistenceContext
    private EntityManager manager;

    public Optional<Integer> getQuestionnaireIdForFillout(int id) {

        Fillout fillout = (Fillout) manager.createQuery("SELECT f FROM Fillout f WHERE f.id = :id")
                .setParameter("id", id)
                .getSingleResult();

        Optional<Integer> result = Optional.of(fillout.getQuestionnaire().getId());
        return result;

    }

    @Override
    public Collection<Fillout> findAll() {
        return manager.createQuery("SELECT f FROM Fillout f")
                .getResultList();
    }

    @Override
    public void deleteById(Integer id) {
        Fillout fillout = manager.find(Fillout.class, id);

        if (fillout != null) {
            manager.remove(fillout);
        }
    }

    @Override
    public void save(Fillout entity) {
        manager.persist(entity);
    }

    @Override
    public void update(Fillout entity) {
        manager.merge(entity);
    }

    @Override
    public Optional<Fillout> findById(Integer id) {
        try {
            Fillout fillout = (Fillout) manager.createQuery("SELECT f FROM Fillout f WHERE f.id= :id")
                    .setParameter("id", id)
                    .getSingleResult();

            return Optional.of(fillout);

        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    //this doesn't find the latest - added as temporary solution
    public Optional<Integer> findByUserId(Integer id) {
        try {
            List<Fillout> filloutList = (List<Fillout>) manager.createQuery("SELECT f FROM Fillout f WHERE f.user.id= :id")
                    .setParameter("id", id)
                    .getResultList();

            return filloutList.stream()
                    .map(f -> f.getId())
                    .findFirst();

        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    @Override
    public int count() {
        return manager.createQuery("SELECT f FROM Fillout f")
                .getResultList().size();
    }
}
