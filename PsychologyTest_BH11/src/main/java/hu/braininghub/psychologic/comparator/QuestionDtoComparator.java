/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.comparator;

import hu.braininghub.psychologic.dto.QuestionDto;
import java.util.Comparator;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Aliz
 */
@ApplicationScoped
public class QuestionDtoComparator implements Comparator<QuestionDto> {

    @Override
    public int compare(QuestionDto a, QuestionDto b) {
        if (a.getRank() == b.getRank()) {
            return 0;   //equal
        }
        if (a.getRank() == null) {
            return 1;
        }
        if (b.getRank() == null) {
            return -1;
        }

        return Integer.compare(a.getRank(), b.getRank());
    }

}
