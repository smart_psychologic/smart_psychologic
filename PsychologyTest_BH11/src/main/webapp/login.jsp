<%-- 
    Document   : login
    Created on : 28-Mar-2020, 08:24:41
    Author     : Marton Petrekanics
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
        <link rel="stylesheet" href="css/signin.css"/>
    </head>
    <body class="text-center">
        <form class="form-signin" action="j_security_check" method="post">
            <img class="mb-4" src="https://i.ebayimg.com/images/g/LKQAAOSwkZJc5BHp/s-l800.jpg" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Bejelentkezés</h1>
            <label for="inputUser" class="sr-only">Felhasználónév</label>
            <input type="text" name="j_username" id="inputUser" class="form-control" placeholder="Felhasználónév" required autofocus>
            <label for="inputPassword" class="sr-only">Jelszó</label>
            <input type="password" name="j_password" id="inputPassword" class="form-control" placeholder="Jelszó" required>
            <div class="checkbox mb-3"></div>
            <button class="btn btn-lg btn-dark btn-block" type="submit">Belépés</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2020 PsychoLogic</p>
        </form>
    </body>
</html>