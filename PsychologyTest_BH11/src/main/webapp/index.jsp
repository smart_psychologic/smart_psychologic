<%-- 
    Document   : categories
    Created on : 2020. febr. 25., 22:20:16
    Author     : Aliz
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Main Page</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
        </style>
        <!-- Custom styles for this template -->
        <link href="css/mainpage.css" rel="stylesheet">

    </head>
    <body onload="alignCenter()">
        <!-- include another JSP page-->
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp"/>

        <main id="container" class="container" style="margin-top: 50px;">
            <h3 class="text-center" style="height: 60px; margin-left: 10px;width: 1110px;">Üdvözlünk!</h3>
            <main role="main">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <ul class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ul>

                    <div class="carousel-inner">

                        <div class="carousel-item active">
                            <img src="https://www.fedsmith.com/wp-content/uploads/2019/05/person-filling-out-questionnaire.jpg" width="1100" height="500">
                        </div>

                        <div class="carousel-item">
                            <img src="https://image.freepik.com/free-photo/closeup-of-person-filling-out-questionary-form_1262-2259.jpg" width="1100" height="500">
                        </div>

                        <div class="carousel-item">
                            <img src="https://a1surveys.com/wp-content/uploads/2019/03/b.jpg"  width="1100" height="500">
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>

                    <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>

                <!-- START THE FEATURETTES -->

                <h2 style="text-align:center; height: 60px; margin-left: 10px;width: 1110px;">A Csapat</h2>
                <div class="row"style="
                     width: 1110px;
                     height: 512px;
                     margin-left: 10px;
                     ">
                    <div class="column">
                        <div class="card">
                            <img src="https://www.kindpng.com/picc/m/294-2947279_user-icon-female-white-passport-size-photo-clipart.png" alt="Aliz" style="width:100%;height: 345px;">
                            <div class="personcontainer" style="height: 340px;">
                                <h2>Molnár Aliz Viktória</h2>
                                <p class="title">Junior Java Developer</p>
                                <p style="
                                   height: 144px;
                                   ">Aliz, a csapat kapitánya, lelkes rajongója a pszichológiai tanulmányoknak.
                                    Leginkább a pszichológiai kérdőívek megvalósítása érdekelte a PsychoLogic projekbten,
                                    amit sikeresen megvalósítottak csapattársaival 2020 márciusában.</p>
                                <p>aliz.prof@gmail.com</p>
                                <a href="contact" class="btn btn-dark btn-block" role="button">Kapcsolat</a>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="card">
                            <img src="https://previews.123rf.com/images/metelsky/metelsky1809/metelsky180900233/109815470-man-avatar-profile-male-face-icon-vector-illustration-.jpg" alt="Marci" style="width:100%;height: 345px;">
                            <div class="personcontainer" style="height: 340px;">
                                <h2>Petrekanics Márton</h2>
                                <p class="title">Junior Java Developer</p>
                                <p style="
                                   height: 144px;
                                   ">Marci a projekt ötletgazdája, és kreatív szakembere. Óriási tapasztalattal rendelkezik mind a pszichológia, mind a fejlesztés terén. </p>
                                <p>p.marci@gmail.com</p>
                                <a href="contact" class="btn btn-dark btn-block" role="button">Kapcsolat</a>
                            </div>
                        </div>
                    </div>

                    <div class="column">
                        <div class="card">
                            <img src="https://previews.123rf.com/images/metelsky/metelsky1809/metelsky180900233/109815470-man-avatar-profile-male-face-icon-vector-illustration-.jpg" alt="Józsi" style="width:100%;height: 345px;">
                            <div class="personcontainer" style="height: 340px;">
                                <h2>Boda József</h2>
                                <p class="title">Junior Java Developer</p>
                                <p style="
                                   height: 144px;
                                   ">Joci 6 éve szoftvertesztelő.
                                    Társaival megalkották a PsychoLogic nevű projektet,
                                    ami alkalmas pszichológia tesztek kitöltésére.</p>
                                <p>j.boda@gmail.com</p>
                                <a href="contact" class="btn btn-dark btn-block" role="button">Kapcsolat</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /END THE FEATURETTES -->



            </main>
            <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>

            <script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
            <script src="js/sidebar.js">

            </script>
    </body>
</html>
