<%-- 
    Document   : contact
    Created on : Mar 24, 2020, 4:01:54 PM
    Author     : mito_qa
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>

        <link href="css/contact.css" rel="stylesheet">
        <link href="css/default.css" rel="stylesheet">

    </head>
    <body onload="alignCenter()">
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp"/>
        <div id="container" class="container">

            <div style="text-align:center">
                <h2>Kapcsolat</h2>
                <p>Ha bármi kérdésed van, írj nekünk nyugodtan:</p>
            </div>
            <div class="row">
                <div class="column">
                    <div id="googleMap" style="width:100%;height:400px;"></div>
                </div>
                <div class="column">

                    <c:if test="${not empty msg}">
                        <c:if test="${isError}">
                            <div class="alert alert-danger" role="alert">
                                <c:out value="${msg}" />
                            </div>
                        </c:if>
                        <c:if test="${not isError}">
                            <div class="alert alert-success" role="alert">
                                <c:out value="${msg}" />
                            </div>
                        </c:if>
                    </c:if>

                    <form method="POST">
                        <label for="firstName">Vezetéknév:</label>
                        <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Vezetékneved..">
                        <label for="lastName">Keresztnév:</label>
                        <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Keresztneved..">
                        <label for="country">Ország:</label>
                        <select id="country" name="country">
                            <option value="Magyarország">Magyarország</option>
                            <option value="ausztria">Ausztria</option>
                            <option value="Csehország">Csehország</option>
                        </select>
                        <label for="message">Üzenet:</label>
                        <textarea id="message" class="form-control" name="message" placeholder="Írj valamit..." style="height:170px"></textarea>
                        <button type="submit" class="btn btn-dark">Küldés</button>
                    </form>
                </div>
            </div>
        </div>

        <script>
            function myMap() {
                var mapProp = {
                    center: new google.maps.LatLng(47.5266817, 19.0653781),
                    zoom: 17,
                };
                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAfI4CoRcQc9J94zPFFkZS1mMHeaoPBQ_s&callback=myMap"></script>

        <script src="js/sidebar.js"></script>
    </body>
</html>
