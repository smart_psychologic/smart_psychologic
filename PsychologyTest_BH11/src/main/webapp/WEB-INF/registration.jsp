<%-- 
    Document   : registration
    Created on : 2020. márc. 28., 19:19:11
    Author     : Aliz
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Registration</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body onload="alignCenter()">
        <!-- include another JSP page-->
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <main id="container" class="container">

            <h4 class="mt-4">Regisztráció</h4>

            <c:if test="${not empty msg}">
                <c:if test="${isError}">
                    <div class="alert alert-danger" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
                <c:if test="${not isError}">
                    <div class="alert alert-success" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
            </c:if>

            <form method="POST">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="last-name">Vezetéknév:</label>
                        <input type="text" class="form-control" id="last-name" name="lastName">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="first-name">Keresztnév:</label>
                        <input type="text" class="form-control" id="first-name" name="firstName">
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Felhasználónév*:</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>  
                <div class="form-group">
                    <label for="email-address">Email cím:</label>
                    <input type="email" class="form-control" id="email-address" name="emailAddress">
                </div>
                <div class="form-group">
                    <label for="password">Jelszó* (min. 5 karakter):</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div> 
                <button type="submit" class="btn btn-outline-dark">Létrehozás</button>
            </form>
        </main>

        <script src="js/sidebar.js"></script>
    </body>
</html>
