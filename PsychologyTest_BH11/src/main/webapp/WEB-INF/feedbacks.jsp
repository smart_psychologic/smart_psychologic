<%-- 
    Document   : feedbacks
    Created on : Mar 30, 2020, 3:19:34 PM
    Author     : mito_qa
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Feedbacks</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
        <link rel="stylesheet" href="css/feedback.css"/>
    </head>
    <body onload="alignCenter()">
        <!-- include another JSP page-->
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <div class="bg">
            <main id="container" class="container">
                <h3 style="
                    margin-bottom: 30px;">Visszajelzések listája:</h3>
                <c:forEach var="feedback" items="${feedbacks}">

                    <button class="accordion"><c:out value="${feedback.id}"/>. visszajelzés</button>
                    <div class="panel">
                        <h3><c:out value="${feedback.firstName} ${feedback.lastName}"/></h3>

                        <p><c:out value="${feedback.country}"/></p>

                        <p><c:out value="${feedback.message}"/></p>
                    </div>

                </c:forEach>  

            </main>
        </div>

        <script src="js/sidebar.js"></script>
        <script src="js/feedback.js"></script>

    </body>

</html>
