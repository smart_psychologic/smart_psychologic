<%-- 
    Document   : create_fillout
    Created on : 2020. márc. 25., 14:52:18
    Author     : Aliz
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List Questions</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body onload="alignCenter()">
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <main id="container" class="container">
            <h3>A kérdőív részletei</h3>

            <div class="card">
                <div class="card-body">
                    <p class="card-text">Neve: <c:out value="${questionaire.name}" /></p>
                    <p class="card-text">Rövid leírás: <c:out value="${questionaire.shortDescription}" /></p>
                    <p class="card-text">Hosszú leírás: <c:out value="${questionaire.longDescription}" /></p>
                    <p class="card-text">Kategória: <c:out value="${questionaire.categoryDTO.name}" /></p>
                </div>
            </div>

            <c:if test="${not empty next_question}">
                <h4 class="mt-4">Kérdőív kitöltése</h4>
                <c:if test="${not empty msg}">
                    <c:if test="${isError}">
                        <div class="alert alert-danger m-2" role="alert">
                            <c:out value="${msg}" />
                        </div>
                    </c:if>
                    <c:if test="${not isError}">
                        <div class="alert alert-success m-2" role="alert">
                            <c:out value="${msg}" />
                        </div>
                    </c:if>
                </c:if>
                <form method="POST">
                    <div class="card mb-3">                   
                        <div class="card-body">
                            <p class="card-text">Sorszám: <c:out value="${next_question.rank}" /></p>
                            <h5 class="card-title"><c:out value="${next_question.text}" /></h5>
                            <c:if test="${not empty next_question.media}">
                                <img src="question-image?id=${next_question.id}" class="img-fluid" alt="Responsive image">
                            </c:if>
                            <input type="hidden" name="question" value="${next_question.id}"/>
                            <c:forEach var="option" items="${options}">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="answer" id="${option}" value="${option}">
                                    <label class="form-check-label" for="${option}">
                                        <c:out value="${option.text}" />
                                    </label>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-outline-dark">Beküldés</button>
                </form>
            </c:if>

            <h4 class="mt-4">Az eddigi válaszok</h4>

            <c:forEach var="answer" items="${answers}">
                <div class="card mb-3">                   
                    <div class="card-body">
                        <p class="card-text">Sorszám: <c:out value="${answer.questionDto.rank}" /></p>
                        <h5 class="card-title"><c:out value="${answer.questionDto.text}" /></h5>
                        <c:if test="${not empty answer.questionDto.media}">
                            <img src="question-image?id=${answer.questionDto.id}" class="img-fluid" alt="Responsive image">
                        </c:if>
                        <p class="card-text">Válasz: <c:out value="${answer.option.text}" /></p>
                    </div>
                </div>
            </c:forEach>
            <script src="js/sidebar.js"></script>
    </body>
</html>
