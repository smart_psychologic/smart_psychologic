<%-- 
    Document   : menubar
    Created on : 2020. febr. 29., 15:10:20
    Author     : Aliz
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8"%>
<div id="myCanvasNav" class="overlay3" onclick="closeNav()" style="width: 0%; opacity: 0;"></div>
<div id="mySidebar" class="sidebar" style="width: 0px">
    <div>
        <img class="logopic" src="https://i.ebayimg.com/images/g/LKQAAOSwkZJc5BHp/s-l800.jpg">
        <a href="home" class="logotext">PsychoLogic</a>
    </div>
    <div class="mb-5"></div>
    <c:if test="${not empty username}">
        <h3 class="text-muted" style="margin-left: 30px;">
            <c:out value="${groupName}"/>: <c:out value="${username}"/>
        </h3>
    </c:if>
    <a href="home" style="margin-left: 30px;">Rólunk</a>
    <c:if test="${empty pageContext.request.remoteUser}">
        <a href="registration" style="margin-left: 30px;">Regisztráció</a>
        <a href="login" style="margin-left: 30px;">Bejelentkezés</a>
    </c:if>
    <a href="create-category" style="margin-left: 30px;">Kategória létrehozása</a>
    <a href="categories" style="margin-left: 30px;">Kategóriák listája</a>
    <a href="create-questionnaire" style="margin-left: 30px;">Kérdőív létrehozása</a>
    <a href="questionnaires" style="margin-left: 30px;">Kérdőívek listája</a>
    <a href="choose-questionaire" style="margin-left: 30px;">Kérdőívek kitöltése</a>
    <c:if test="${not empty username}">
        <a href="evaluation" style="margin-left: 30px;">Kérdőíveim eredményei</a>
    </c:if>
    <a href="contact" style="margin-left: 30px;">Kapcsolat</a>
    <a href="feedbacks" style="margin-left: 30px;">Visszajelzések</a> 
    <c:if test="${not empty username}">
        <a href="logout" style="margin-left: 30px;">Kijelentkezés</a>
    </c:if>
</div>

<div id="main" style="margin-left: 0px">
    <button class="openbtn" onclick="toggleNav()">☰</button>  
</div>
