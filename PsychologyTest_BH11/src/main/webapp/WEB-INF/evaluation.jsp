<%-- 
    Document   : evaluation
    Created on : 29-Mar-2020, 14:55:22
    Author     : Marton Petrekanics
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>

        <!--<link href="css/contact.css" rel="stylesheet">-->
        <link href="css/default.css" rel="stylesheet">

    </head>
    <body onload="alignCenter()">
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp"/>
        <div id="container" class="container">
            <div style="text-align:center">
                <h2>Kiértékelések</h2>
                <p>A legutóbbi kérdőíved <c:out value='${requestScope.questionnaireName}'/> 
                    kitöltésének eredménye:</p>
                <h1><c:out value='${requestScope.result}'/></h1>
            </div>

        </div>

        <script>

        </script>

        <script src="js/sidebar.js"></script>
    </body>
</html>
