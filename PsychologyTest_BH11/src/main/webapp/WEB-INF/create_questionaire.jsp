<%-- 
    Document   : create_q
    Created on : 2020. febr. 29., 15:04:51
    Author     : Aliz
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Create Questionnaire</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body onload="alignCenter()">
        <!-- include another JSP page-->
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <main id="container" class="container">
            <c:if test="${not empty msg}">
                <c:if test="${isError}">
                    <div class="alert alert-danger" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
                <c:if test="${not isError}">
                    <div class="alert alert-success" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
            </c:if>
            <form method="POST">
                <div class="form-group">
                    <label for="name">Kérdőív neve:</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group">
                    <label for="short-desc">Kérdőív rövid leírása:</label>
                    <input type="text" class="form-control" id="short-desc" name="short-desc">
                </div>
                <div class="form-group">
                    <label for="long-desc">Kérdőív bővebb leírása:</label>
                    <textarea class="form-control" id="long-desc" rows="3" name="long-desc"></textarea>
                </div>
                <div class="form-group">
                    <label for="category">Kategória kiválasztása</label>
                    <select name="category"
                            class="form-control"
                            id="category">
                        <c:forEach  var="category" items="${categories}">
                            <option value="${category.id}"><c:out value="${category.name}"/></option>
                        </c:forEach>
                    </select>
                </div>               
                <button type="submit" class="btn btn-outline-dark">Létrehozás</button>
            </form>
        </main>

        <script src="js/sidebar.js"></script>
    </body>
</html>

