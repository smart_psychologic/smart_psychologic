<%-- 
    Document   : view_question_list
    Created on : 2020. febr. 29., 14:52:18
    Author     : Aliz
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List Questions</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body onload="alignCenter()">
        <!-- include another JSP page-->
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <main id="container" class="container">
            <h3>A kérdőív részletei</h3>

            <div class="card">
                <div class="card-body">
                    <p class="card-text">Neve: <c:out value="${questionaire.name}" /></p>
                    <p class="card-text">Rövid leírás: <c:out value="${questionaire.shortDescription}" /></p>
                    <p class="card-text">Hosszú leírás: <c:out value="${questionaire.longDescription}" /></p>
                    <p class="card-text">Kategória: <c:out value="${questionaire.categoryDTO.name}" /></p>
                </div>
            </div>

            <h4 class="mt-4">Kérdések létrehozása</h4>

            <c:if test="${not empty msg}">
                <c:if test="${isError}">
                    <div class="alert alert-danger" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
                <c:if test="${not isError}">
                    <div class="alert alert-success" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
            </c:if>

            <form method="POST" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="rank">Kérdés sorszáma:</label>
                    <input type="text" class="form-control" id="rank" name="rank">
                </div>
                <div class="form-group">
                    <label for="text">Kérdés szövege:</label>
                    <input type="text" class="form-control" id="text" name="text">
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="media">Média beszúrása:</span>
                        </div>                  
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="file" name="media"
                                   aria-describedby="media">
                            <label class="custom-file-label" for="file" data-browse="Kiválaszt">Fájl kiválasztása</label>
                        </div>                  
                    </div>
                </div>
                <button type="submit" class="btn btn-outline-dark">Létrehozás</button>

            </form>

            <h4 class="mt-4">A kérdések</h4>
            <c:forEach var="question" items="${questionaire.questionDto}">
                <div class="card mb-3">                   
                    <div class="card-body">
                        <p class="card-text">Sorszám: <c:out value="${question.rank}" /></p>
                        <h5 class="card-title"><c:out value="${question.text}" /></h5>
                        <c:if test="${not empty question.media}">
                            <img src="question-image?id=${question.id}" class="img-fluid" alt="Responsive image">
                        </c:if>                      
                    </div>
                </div>
            </c:forEach>
        </main>
        <script src="js/sidebar.js"></script>
    </body>
</html>
