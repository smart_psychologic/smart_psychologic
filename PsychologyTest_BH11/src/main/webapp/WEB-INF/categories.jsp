<%-- 
    Document   : categories
    Created on : 2020. febr. 25., 22:20:16
    Author     : Aliz
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List Categories</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body onload="alignCenter()">
        <!-- include another JSP page-->
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <div class="bg">
            <main id="container" class="container">
                <h3>Kategórák listája:</h3>
                <ul class="list-group">
                    <c:forEach var="category" items="${categories}">
                        <li class="list-group-item"><c:out value="${category.name}" /></li>
                        </c:forEach>  
                </ul>
            </main>
        </div>

        <script src="js/sidebar.js"></script>
    </body>

</html>
