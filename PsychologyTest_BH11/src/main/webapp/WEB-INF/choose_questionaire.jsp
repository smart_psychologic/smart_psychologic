<%-- 
    Document   : choose_questionaire
    Created on : 2020. márc. 25., 13:44:29
    Author     : Aliz
--%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Choose Questionaire</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body onload="alignCenter()">
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <main id="container" class="container">
            <h3>Kérdőív kiválasztása kitöltéshez:</h3>

            <c:forEach var="q" items="${q_dtos}">
                <div class="card mb-3">
                    <div class="card-body">
                        <h5 class="card-title"><c:out value="${q.name}" /></h5>
                        <p class="card-text"><c:out value="${q.shortDescription}" /></p>
                        <p class="card-text"><small class="text-muted"><c:out value="${q.categoryDTO.name}" /></small></p>
                        <!--link to CreateFilloutServlet-->
                        <a href="choose-options?id=${q.id}" class="btn btn-dark">Kitöltés...</a>    
                    </div>
                </div>
            </c:forEach>
        </main>
        <script src="js/sidebar.js"></script>
    </body>
</html>
