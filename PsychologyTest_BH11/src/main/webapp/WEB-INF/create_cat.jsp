<%-- 
    Document   : create_cat
    Created on : 2020. febr. 29., 16:19:17
    Author     : Aliz
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Create Category</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body onload="alignCenter()">
        <!-- include another JSP page-->
        <jsp:include page="/WEB-INF/jsp_include/menubar.jsp" />
        <main id="container" class="container">
            <c:if test="${not empty msg}">
                <c:if test="${isError}">
                    <div class="alert alert-danger" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
                <c:if test="${not isError}">
                    <div class="alert alert-success" role="alert">
                        <c:out value="${msg}" />
                    </div>
                </c:if>
            </c:if>

            <form method="POST">
                <div class="form-group">
                    <label for="name">Kategória neve:</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>                
                <button type="submit" class="btn btn-outline-dark">Létrehozás</button>
            </form>
        </main>

        <script src="js/sidebar.js"></script>
    </body>
</html>