<%-- 
    Document   : error
    Created on : 28-Mar-2020, 08:25:23
    Author     : Marton Petrekanics
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
-<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Login error</title>
        <jsp:include page="/WEB-INF/jsp_include/html_head.jsp"/>
        <link rel="stylesheet" href="css/default.css"/>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron mt-3">
                <h1>Hibás bejelentkezés</h1>
                <p class="lead">Hibás Felhasználónév vagy a jelszó.</p>
                <a class="btn btn-lg btn-outline-dark" href="#" onclick="goBack()" role="button">Vissza a bejelentkező oldalra</a>
            </div>
            <script>
                function goBack() {
                    window.history.back();
                }
            </script>
        </div>
    </body>
</html>
