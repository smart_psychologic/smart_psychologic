/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.DimensionDto;
import hu.braininghub.psychologic.model.Dimension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Marton Petrekanics
 */
@ExtendWith(MockitoExtension.class)
public class TestDimensionMapper {

    private DimensionMapper underTest;

    @BeforeEach
    void init() {
        underTest = new DimensionMapper();
    }

    @Test
    void transitionMapperTest() {
        //Given
        Dimension dimension = new Dimension();
        dimension.setId(5);
        dimension.setFifthText("test fifth");
        dimension.setThirdBoundary(60);

        //When
        DimensionDto dimensionDTO = underTest.toDto(dimension);

        //transition back
        Dimension result = underTest.toEntity(dimensionDTO);
        //Then
        Assertions.assertEquals(5, result.getId()); //expected, actual        for int type
        Assertions.assertEquals("test fifth", result.getFifthText()); //expected, actual        for int type
        Assertions.assertEquals(60, result.getThirdBoundary()); //expected, actual        for int type
    }
}
