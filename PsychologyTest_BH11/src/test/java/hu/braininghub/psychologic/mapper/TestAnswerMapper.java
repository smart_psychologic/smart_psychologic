/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.AnswerDto;
import hu.braininghub.psychologic.model.Answer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Marton Petrekanics
 */
@ExtendWith(MockitoExtension.class)
public class TestAnswerMapper {

    private AnswerMapper underTest;

    @BeforeEach
    void init() {
        underTest = new AnswerMapper();
    }

    @Test
    void transitionMapperTest() {
        //Given
        Answer answer = new Answer();
        answer.setId(5);

        //When
        AnswerDto answerDTO = underTest.toDto(answer);

        //transition back
        Answer result = underTest.toEntity(answerDTO);
        //Then
        Assertions.assertEquals(Integer.valueOf(5), result.getId()); //expected, actual        for int type
    }
}
