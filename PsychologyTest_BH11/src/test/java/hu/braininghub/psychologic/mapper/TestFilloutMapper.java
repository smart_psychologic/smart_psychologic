/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.FilloutDto;
import hu.braininghub.psychologic.model.Fillout;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Marton Petrekanics
 */
@ExtendWith(MockitoExtension.class)
public class TestFilloutMapper {

    private FilloutMapper underTest;

    @BeforeEach
    void init() {
        underTest = new FilloutMapper();
    }

    @Test
    void transitionMapperTest() {
        //Given
        Fillout fillout = new Fillout();
        fillout.setId(5);
        fillout.setFilloutDuration(1.5);

        //When
        FilloutDto filloutDTO = underTest.toDto(fillout);

        //transition back
        Fillout result = underTest.toEntity(filloutDTO);
        //Then
        Assertions.assertEquals(5, result.getId()); //expected, actual        for int type
    }
}
