/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.OptionDto;
import hu.braininghub.psychologic.model.Option;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Marton Petrekanics
 */
@ExtendWith(MockitoExtension.class)
public class TestOptionMapper {

    private OptionMapper underTest;

    @BeforeEach
    void init() {
        underTest = new OptionMapper();
    }

    @Test
    void transitionMapperTest() {
        //Given
        Option option = new Option();
        option.setId(5);
        option.setRank(3);
        option.setText("test text");
        option.setValue(1000);

        //When
        OptionDto optionDTO = underTest.toDto(option);

        //transition back
        Option result = underTest.toEntity(optionDTO);
        //Then
        Assertions.assertEquals(5, result.getId()); //expected, actual        for int type
        Assertions.assertEquals(3, result.getRank()); //expected, actual        for int type
        Assertions.assertEquals("test text", result.getText()); //expected, actual        for int type
        Assertions.assertEquals(1000, result.getValue()); //expected, actual        for int type
    }
}
