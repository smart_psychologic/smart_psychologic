/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.UserDto;
import hu.braininghub.psychologic.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Marton Petrekanics
 */
@ExtendWith(MockitoExtension.class)
public class TestUserMapper {

    private UserMapper underTest;

    @BeforeEach
    void init() {
        underTest = new UserMapper();
    }

    @Test
    void transitionMapperTest() {
        //Given
        User user = new User();
        user.setId(5);
        user.setEmailAddress("test email");
        user.setPassword("test password");

        //When
        UserDto userDTO = underTest.toDto(user);

        //transition back
        User result = underTest.toEntity(userDTO);
        //Then
        Assertions.assertEquals(5, result.getId()); //expected, actual        for int type
        Assertions.assertEquals("test email", result.getEmailAddress()); //expected, actual        for int type
        Assertions.assertEquals("test password", result.getPassword()); //expected, actual        for int type
    }
}
