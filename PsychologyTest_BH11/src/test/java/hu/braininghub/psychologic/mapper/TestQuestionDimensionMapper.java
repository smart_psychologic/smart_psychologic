/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.QuestionDimensionDto;
import hu.braininghub.psychologic.model.QuestionDimension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Marton Petrekanics
 */
@ExtendWith(MockitoExtension.class)
public class TestQuestionDimensionMapper {

    private QuestionDimensionMapper underTest;

    @BeforeEach
    void init() {
        underTest = new QuestionDimensionMapper();
    }

    @Test
    void transitionMapperTest() {
        //Given
        QuestionDimension questionDimension = new QuestionDimension();
        questionDimension.setId(5);

        //When
        QuestionDimensionDto questionDimensionDTO = underTest.toDto(questionDimension);

        //transition back
        QuestionDimension result = underTest.toEntity(questionDimensionDTO);
        //Then
        Assertions.assertEquals(5, result.getId()); //expected, actual        for int type
    }
}
