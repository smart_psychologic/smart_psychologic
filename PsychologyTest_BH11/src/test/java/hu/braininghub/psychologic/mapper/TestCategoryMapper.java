/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.mapper;

import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.model.Category;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Aliz
 */
public class TestCategoryMapper {

    private CategoryMapper underTest;

    @BeforeEach
    void init() {
        underTest = new CategoryMapper();
    }

    @Test
    void transitionMapperTest() {
        //Given
        Category category = new Category();
        category.setId(Integer.valueOf(5));
        category.setName("test");

        //When
        CategoryDto categoryDTO = underTest.toDto(category);

        //transition back
        Category result = underTest.toEntity(categoryDTO);
        //Then
        Assertions.assertEquals(Integer.valueOf(5), result.getId()); //expected, actual        for int type
        Assertions.assertEquals("test", result.getName());  //for String type
    }
}
