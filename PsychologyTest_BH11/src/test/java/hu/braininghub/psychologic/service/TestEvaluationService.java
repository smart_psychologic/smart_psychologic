/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.controller.AnswerDao;
import hu.braininghub.psychologic.controller.DimensionDao;
import hu.braininghub.psychologic.controller.FilloutDao;
import hu.braininghub.psychologic.controller.OptionDao;
import hu.braininghub.psychologic.controller.QuestionDao;
import hu.braininghub.psychologic.controller.QuestionDimensionDao;
import hu.braininghub.psychologic.dto.AnswerDto;
import hu.braininghub.psychologic.dto.DimensionDto;
import hu.braininghub.psychologic.dto.OptionDto;
import hu.braininghub.psychologic.dto.QuestionDimensionDto;
import hu.braininghub.psychologic.mapper.AnswerMapper;
import hu.braininghub.psychologic.mapper.DimensionMapper;
import hu.braininghub.psychologic.mapper.OptionMapper;
import hu.braininghub.psychologic.mapper.QuestionDimensionMapper;
import hu.braininghub.psychologic.model.Answer;
import hu.braininghub.psychologic.model.Dimension;
import hu.braininghub.psychologic.model.Option;
import hu.braininghub.psychologic.model.QuestionDimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Marton Petrekanics
 */
@ExtendWith(MockitoExtension.class)
public class TestEvaluationService {

    @Mock
    private QuestionDao questionDao;

    @Mock
    private FilloutDao filloutDao;

    @Mock
    private AnswerDao answerDao;

    @Mock
    private DimensionDao dimensionDao;

    @Mock
    private OptionDao optionDao;

    @Mock
    private QuestionDimensionDao questionDimensionDao;

    @Mock
    private OptionMapper optionMapper;

    @Mock
    private QuestionDimensionMapper questionDimensionMapper;

    @Mock
    private AnswerMapper answerMapper;

    @Mock
    private DimensionMapper dimensionMapper;

    private EvaluationService underTest;

    @BeforeEach
    void init() {
        underTest = new EvaluationService();
        underTest.setAnswerMapper(answerMapper);
        underTest.setQuestionDimensionDao(questionDimensionDao);
        underTest.setOptionDao(optionDao);
        underTest.setAnswerDao(answerDao);
        underTest.setFilloutDao(filloutDao);
        underTest.setDimensionDao(dimensionDao);
        underTest.setQuestionDao(questionDao);
        underTest.setDimensionMapper(dimensionMapper);
        underTest.setOptionMapper(optionMapper);
        underTest.setQuestionDimensionMapper(questionDimensionMapper);
    }

    @Test
    void testEvaluateQuestionnaire() {
        //Given
        int filloutId = 3;
        int questionId = 77;
        int questionnaireIdRaw = 15;
        int questionDimensionId = 88;
        int answerId = 100;
        int optionId = 300;
        int dimensionId = 777;

        Optional<Integer> questionnaireId = Optional.of(questionnaireIdRaw);
        Mockito.when(filloutDao.getQuestionnaireIdForFillout(filloutId)).thenReturn(questionnaireId);

        List<Integer> questionIds = new ArrayList();
        questionIds.add(questionId);
        Mockito.when(questionDao.getAllQuestionIdsForQuestionnaire(questionnaireIdRaw)).thenReturn(questionIds);

        AnswerDto answerDTO = Mockito.mock(AnswerDto.class);
        Answer answer = Mockito.mock(Answer.class);
        List<Answer> answerList = new ArrayList();
        answerList.add(answer);
        Mockito.when(answerDao.getAnswersForFillout(filloutId)).thenReturn(answerList);

        DimensionDto dimensionDTO = Mockito.mock(DimensionDto.class);
        Dimension dimension = Mockito.mock(Dimension.class);
        List<Dimension> dimensionList = new ArrayList();
        dimensionList.add(dimension);
        Mockito.when(dimensionDao.getAllDimensionsForQuestionnaire(questionnaireIdRaw)).thenReturn(dimensionList);

        OptionDto optionDTO = Mockito.mock(OptionDto.class);
        Option option = Mockito.mock(Option.class);
        List<Option> optionList = new ArrayList();
        optionList.add(option);
        Mockito.when(optionDao.getOptionsForQuestionnaire(questionnaireIdRaw)).thenReturn(optionList);

        QuestionDimensionDto questionDimensionDTO = Mockito.mock(QuestionDimensionDto.class);
        QuestionDimension questionDimension = Mockito.mock(QuestionDimension.class);
        List<QuestionDimension> questionDimensionList = new ArrayList();
        questionDimensionList.add(questionDimension);
        Mockito.when(questionDimensionDao.getAllQuestionDimensionsForQuestion(questionId))
                .thenReturn(questionDimensionList);

        Mockito.when(answerMapper.toDto(answer)).thenReturn(answerDTO);
        Mockito.when(dimensionMapper.toDto(dimension)).thenReturn(dimensionDTO);
        Mockito.when(optionMapper.toDto(option)).thenReturn(optionDTO);
        Mockito.when(questionDimensionMapper.toDto(questionDimension))
                .thenReturn(questionDimensionDTO);

        Mockito.when(dimensionDTO.getId()).thenReturn(dimensionId);
        Mockito.when(answerDTO.getId()).thenReturn(answerId);
        Mockito.when(optionDTO.getId()).thenReturn(optionId);
        Mockito.when(optionDTO.getValue()).thenReturn(50);
        Mockito.when(questionDimensionDTO.getId()).thenReturn(questionDimensionId);
        Mockito.when(dimensionDTO.getFirstBoundary()).thenReturn(30);
        Mockito.when(dimensionDTO.getSecondBoundary()).thenReturn(60);
        Mockito.when(dimensionDTO.getSecondText()).thenReturn("Second text test text.");

        Mockito.when(questionDimensionDao.getQuestionIdForQuestionDimension(questionDimensionId))
                .thenReturn(Optional.of(questionId));
        Mockito.when(answerDao.getQuestionIdForAnswer(answerId)).thenReturn(Optional.of(questionId));
        Mockito.when(answerDao.getOptionIdForAnswer(answerId)).thenReturn(Optional.of(optionId));
        Mockito.when(questionDimensionDao.getDimensionIdForQuestionDimension(questionDimensionId))
                .thenReturn(Optional.of(dimensionId));

        //When
        String result = "";
        try {
            result = underTest.evaluateQuestionnaire(3);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Then - test
        Assertions.assertTrue("Second text test text.".equals(result));

    }

    @Test
    void testEvaluateQuestionnaireMissingQuestionnaireId() {
        //Given
        Mockito.when(filloutDao.getQuestionnaireIdForFillout(3)).thenReturn(Optional.empty());

        String expectedMessage = "Missing questionnaireId.";

        //When
        //Then
        Exception exception = Assertions.assertThrows(EvaluationException.class,
                () -> underTest.evaluateQuestionnaire(3));

        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));

    }

    @Test
    void testEvaluateQuestionnaireWhenNoAnswers() {

        //Given
        Mockito.when(filloutDao.getQuestionnaireIdForFillout(3)).thenReturn(Optional.of(15));
        Mockito.when(answerDao.getAnswersForFillout(3)).thenReturn(new ArrayList());

        String expectedMessage = "No answers for this fillout.";

        //When
        //Then
        Exception exception = Assertions.assertThrows(EvaluationException.class,
                () -> underTest.evaluateQuestionnaire(3));

        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testEvaluateQuestionnaireWhenNoDimensions() {

        //Given
        Mockito.when(filloutDao.getQuestionnaireIdForFillout(3)).thenReturn(Optional.of(15));

        AnswerDto answerDTO = Mockito.mock(AnswerDto.class);
        Answer answer = Mockito.mock(Answer.class);
        List<Answer> answerList = new ArrayList();
        answerList.add(answer);
        Mockito.when(answerDao.getAnswersForFillout(3)).thenReturn(answerList);
        Mockito.when(answerMapper.toDto(answer)).thenReturn(answerDTO);

        Mockito.when(dimensionDao.getAllDimensionsForQuestionnaire(15)).thenReturn(new ArrayList());

        String expectedMessage = "No dimensions for this questionnaire.";

        //When
        //Then
        Exception exception = Assertions.assertThrows(EvaluationException.class,
                () -> underTest.evaluateQuestionnaire(3));

        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void testEvaluateQuestionnaireWhenNoQuestions() {

        //Given
        Mockito.when(filloutDao.getQuestionnaireIdForFillout(3)).thenReturn(Optional.of(15));

        AnswerDto answerDTO = Mockito.mock(AnswerDto.class);
        Answer answer = Mockito.mock(Answer.class);
        List<Answer> answerList = new ArrayList();
        answerList.add(answer);
        Mockito.when(answerDao.getAnswersForFillout(3)).thenReturn(answerList);
        Mockito.when(answerMapper.toDto(answer)).thenReturn(answerDTO);

        DimensionDto dimensionDTO = Mockito.mock(DimensionDto.class);
        Dimension dimension = Mockito.mock(Dimension.class);
        List<Dimension> dimensionList = new ArrayList();
        dimensionList.add(dimension);
        Mockito.when(dimensionDao.getAllDimensionsForQuestionnaire(15)).thenReturn(dimensionList);
        Mockito.when(dimensionMapper.toDto(dimension)).thenReturn(dimensionDTO);

        Mockito.when(questionDao.getAllQuestionIdsForQuestionnaire(15)).thenReturn(new ArrayList());

        String expectedMessage = "Missing questions.";

        //When
        //Then
        Exception exception = Assertions.assertThrows(EvaluationException.class,
                () -> underTest.evaluateQuestionnaire(3));

        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

}
