/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.service;

import hu.braininghub.psychologic.controller.CategoryDao;
import hu.braininghub.psychologic.controller.QuestionaireDao;
import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.dto.QuestionaireDto;
import hu.braininghub.psychologic.mapper.CategoryMapper;
import hu.braininghub.psychologic.mapper.QuestionaireMapper;
import hu.braininghub.psychologic.model.Category;
import hu.braininghub.psychologic.model.Questionaire;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Aliz
 */
@ExtendWith(MockitoExtension.class)
public class TestPsychologyService {

    @Mock
    private CategoryMapper categoryMapper;

    @Mock
    private QuestionaireMapper qMapper;

    @Mock
    private CategoryDao categoryDao;

    @Mock
    private QuestionaireDao questionaireDao;

    private PsychologyService underTest;

    @BeforeEach
    void init() {
        underTest = new PsychologyService();
        underTest.setCategoryDao(categoryDao);
        underTest.setQuestionaireDao(questionaireDao);
        underTest.setCategoryMapper(categoryMapper);
        underTest.setQuestionaireMapper(qMapper);
    }

    @Test
    void testAddCategory() {

        //Given - prepare
        CategoryDto categoryDTO = Mockito.mock(CategoryDto.class);

        Category category = Mockito.mock(Category.class);
        Mockito.when(categoryMapper.toEntity(categoryDTO)).thenReturn(category);
        //When - try
        //simulation
        boolean result = underTest.addCategory(categoryDTO);
        //Then - test
        Assertions.assertTrue(result);
    }

    @Test
    void testAddCategorywithException() {

        //Given - prepare
        CategoryDto categoryDTO = Mockito.mock(CategoryDto.class);

        Category category = Mockito.mock(Category.class);
        Mockito.when(categoryMapper.toEntity(categoryDTO)).thenReturn(category);
        //simulate void dao.addCategory() throws Exception
        Mockito.doThrow(new RuntimeException()).when(categoryDao).addCategory(category);
        //When - try
        boolean result = underTest.addCategory(categoryDTO);
        //Then - test
        Assertions.assertFalse(result);
    }

    @Test
    void testGetAllCategoryWith1Item() {
        //Given
        List<Category> categories = new ArrayList<>();
        Category category = Mockito.mock(Category.class);
        categories.add(category);
        Mockito.when(categoryDao.listAllCategories()).thenReturn(categories);

        CategoryDto categoryDTO = Mockito.mock(CategoryDto.class);
        Mockito.when(categoryMapper.toDto(category)).thenReturn(categoryDTO);

        //When
        List<CategoryDto> dtos = underTest.getAllCategories();
        //Then

        //expected 1 item in dtos
        Assertions.assertEquals(1, dtos.size());
        //expected name is the same as DTO
        Assertions.assertSame(categoryDTO, dtos.get(0));
    }

    @Test
    void testGetAllCategoryWithException() {
        //Given
        Mockito.when(categoryDao.listAllCategories()).thenThrow(new RuntimeException());

        //When
        List<CategoryDto> dtos = underTest.getAllCategories();
        //Then
        Assertions.assertEquals(new ArrayList<CategoryDto>(), dtos);

    }

    @Test
    void testGetQuestionaireNames() {
        //Given
        Questionaire questionaire = Mockito.mock(Questionaire.class);
        Mockito.when(questionaire.getName()).thenReturn("TestName");
        Mockito.when(questionaireDao.getAllQuestionaires()).thenReturn(Arrays.asList(questionaire));
        //When
        List<String> result = underTest.getQuestionaireNames();
        //Then
        Assertions.assertEquals(Arrays.asList("TestName"), result);

    }

    @Test
    void testAddQuestionaire() {

        //Given
        QuestionaireDto questionaireDTO = Mockito.mock(QuestionaireDto.class);

        Questionaire questionaire = Mockito.mock(Questionaire.class);
        Mockito.when(qMapper.toEntity(questionaireDTO)).thenReturn(questionaire);
        //When
        boolean result = underTest.addQuestionaire(questionaireDTO);
        //Then
        Assertions.assertEquals(true, result);
    }

    @Test
    void testAddQuestionaireWithException() {

        //Given
        QuestionaireDto questionaireDTO = Mockito.mock(QuestionaireDto.class);

        Questionaire questionaire = Mockito.mock(Questionaire.class);
        Mockito.when(qMapper.toEntity(questionaireDTO)).thenReturn(questionaire);
        //simulate void dao.addQuestionnaire() throws Exception
        Mockito.doThrow(new RuntimeException()).when(questionaireDao).addQuestionnaire(questionaire);
        //When - try
        boolean result = underTest.addQuestionaire(questionaireDTO);
        //Then - test
        Assertions.assertFalse(result); //expected that adding is not successful
        Mockito.verify(questionaireDao, Mockito.times(1)).addQuestionnaire(questionaire);   //Verifiing underTest calls addQuestionnaire() 1x from dao
    }
}
