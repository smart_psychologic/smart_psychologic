/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.braininghub.psychologic.servlet;

import hu.braininghub.psychologic.dto.CategoryDto;
import hu.braininghub.psychologic.service.PsychologyService;
import hu.braininghub.psychologic.validation.QuestionaireDtoValidator;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

/**
 *
 * @author Aliz
 */
@ExtendWith(MockitoExtension.class)
public class TestCreateQuestionaireServlet {

    @Mock
    private PsychologyService psychologyService;

    @Mock
    private QuestionaireDtoValidator testQDTOValidator;

    private CreateQuestionaireServlet underTest;

    @BeforeEach
    void init() {
        underTest = new CreateQuestionaireServlet();
        underTest.setPsychologyService(psychologyService);
        underTest.setQuestionaireDTOValidator(testQDTOValidator);
    }

    @Test
    void testDoGet() throws ServletException, IOException {

        //Given - prepare
        CategoryDto categoryDTO = Mockito.mock(CategoryDto.class);
        List<CategoryDto> categoryDTOList = new ArrayList<>();

        categoryDTOList.add(categoryDTO);
        Mockito.when(psychologyService.getAllCategories()).thenReturn(categoryDTOList);

        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        RequestDispatcher dispathcer = Mockito.mock(RequestDispatcher.class);
        Mockito.when(request.getRequestDispatcher("WEB-INF/create_questionaire.jsp")).thenReturn(dispathcer);
        //When - try
        underTest.doGet(request, response);
        //Then - test
        /*
        request object is checked, if setAttribute() has been called once, and 
        if attribute equals given String and referenced same List
         */
        Mockito.verify(request, Mockito.times(1)).setAttribute(Mockito.eq("categories"), Mockito.same(categoryDTOList));

    }

}
